<header>
      <div id="header_background">
        <div class="main container-fluid" style="margin: auto;">
          <nav class="navi-area no-margin">
            <div class="logo-area"><a href="#"><img src="<?php echo get_option('est_logo'); ?>" alt="Logo"></a></div>
            <div class="right-area"><a href="#"><img src="<?php echo get_option('est_phone_icon'); ?>" style="margin-right: 10px;" alt=""><span>+90</span><?php echo get_option('est_phone'); ?></a><a class="btn-appointment" href="<?php echo get_option('est_free_app_url') ?>">Free Appointment<i data-feather="arrow-right-circle"></i></a></div>
          </nav>
        </div>
        <?php get_template_part('inc/navbar_menus'); ?>
        <div class="container-fluid">
          <div class="header-hero-title"><span class="category"><?php echo get_option('est_home_slider_top_button') ?></span>
            <h1><?php echo get_option('est_home_slider_slogan') ?></span></h1><a class="button pink" href="<?php echo get_option('est_home_slider_bottom_button_url') ?>"><?php echo get_option('est_home_slider_bottom_button') ?><i data-feather="arrow-right-circle"></i></a>
          </div>
        </div>

        <!-- board slider-->
        <div class="board--items">
          <ul>
          <?php
    // Get the ID of a given category
    $face_id = get_cat_ID( 'face' );
    $face_link = get_category_link( $face_id );

    $body_id = get_cat_ID( 'body' );
    $body_link = get_category_link( $body_id );



    $breast_id = get_cat_ID( 'breast' );
    $breast_link = get_category_link( $breast_id );

    $genital_id = get_cat_ID( 'genital' );
    $genital_link = get_category_link( $genital_id );

    $nose_id = get_cat_ID( 'nose' );
    $nose_link = get_category_link( $nose_id );

    $liposuction_id = get_cat_ID( 'liposuction' );
    $liposuction_link = get_category_link( $liposuction_id );

    $butt_id = get_cat_ID( 'butt' );
    $butt_link = get_category_link( $butt_id );

    ?>


            <li class="visible-mobile"><a href="<?php echo esc_url( $face_link); ?>">
                <figure itemprop="image" itemscope itemtype="image"><img src="<?php bloginfo('template_url'); ?>/dist/img/board/face.svg" alt="face"></figure><span>Face</span></a></li>
            <li class="visible-mobile"><a href="<?php echo esc_url( $body_link); ?>">
                <figure itemprop="image" itemscope itemtype="image"><img src="<?php bloginfo('template_url'); ?>/dist/img/board/body.svg" alt="body"></figure><span>Body</span></a></li>
            <li class="visible-mobile"><a href="<?php echo esc_url( $butt_link); ?>">
                <figure itemprop="image" itemscope itemtype="image"><img src="<?php bloginfo('template_url'); ?>/dist/img/board/butt.svg" alt="butt"></figure><span>Butt Reshaping</span></a></li>
            <li><a href="<?php echo esc_url( $breast_link); ?>">
                <figure itemprop="image" itemscope itemtype="image"><img src="<?php bloginfo('template_url'); ?>/dist/img/board/breast.svg" alt="breast"></figure><span>Breast</span></a></li>
            <li><a href="<?php echo esc_url( $genital_link); ?>">
                <figure itemprop="image" itemscope itemtype="image"><img src="<?php bloginfo('template_url'); ?>/dist/img/board/genital.svg" alt="genital"></figure><span>Genital</span></a></li>
            <li><a href="<?php echo esc_url( $nose_link); ?>">
                <figure itemprop="image" itemscope itemtype="image"><img src="<?php bloginfo('template_url'); ?>/dist/img/board/nose.svg" alt="nose"></figure><span>Nose</span></a></li>
            <li><a href="<?php echo esc_url( $liposuction_link); ?>">
                <figure itemprop="image" itemscope itemtype="image"><img src="<?php bloginfo('template_url'); ?>/dist/img/board/liposuction.svg" alt="liposuction"></figure><span>Liposuction</span></a></li>
          </ul>
        </div>
      </div>
    </header>
