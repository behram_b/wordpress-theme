<?php get_template_part('head'); ?>
<?php get_template_part('inc/testimonials/detail/header'); ?>

<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
    if (!empty(get_the_content())) {
        the_content();
    }
    ?>

<?php endwhile; else: ?>
    <p><?php _e('Bulunamadı'); ?></p>
<?php endif; ?>

<?php get_template_part('footer'); ?>
