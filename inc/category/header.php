<header class="main-header mb " style="background: url(<?php bloginfo('template_url'); ?>/dist/img/bg/category.jpg);">
      <div class="container-fluid">
        <nav class="navi-area">
          <div class="logo-area"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_option('est_logo'); ?>" alt="Logo"></a></div>
          <div class="right-area"><a href="#"><img src="<?php echo get_option('est_phone_icon'); ?>" style="margin-right: 10px;" alt=""><span>+90</span><?php echo get_option('est_phone'); ?></a><a class="btn-appointment" href="#">Free Appointment<i data-feather="arrow-right-circle"></i></a></div>
        </nav>
      </div>
      <?php get_template_part('inc/navbar_menus'); ?>
      <div class="container-fluid">
        <!-- hero title-->
        <div class="hero-title">
        <h1 class="color-white">Categories</h1>
        <p><?php the_excerpt(); ?></p>
          <!-- info buttons-->
        </div>
        <ul class="detail-buttons mt-50">
          <li><a href="#"><b>Online</b><small>Consultation</small></a></li>
          <li><a href="#"><b>Call Me</b><small>Back</small></a></li>
        </ul>
      </div>
    </header>


    