<!-- page content-->
<div class="container-fluid">
      <div class="row">
        <div class="col-lg-3 col-lg-push-9 col-md-push-8 col-md-4 col-xs-12 col-xs-padding-0">
          <div class="contact-left is-small cover-bg" style="background: url(/wp-content/uploads/2018/03/contact-form-3.jpg)">
            <h6>Call Me Back<i data-feather="arrow-down-circle"></i></h6>
            <form action="<?php bloginfo('template_url'); ?>/form/form.php" method="post" id="form_send">
            <div id="form">
            <div class="form-group">
            <select name="procedure" id="procedure" class="select form-control" required>              
              <option value="">Select&nbsp;Treatment</option>
              <option value="Spiderweb Aesthetic">Spiderweb Aesthetic</option>
              <option value="Organic&nbsp;Hair Transplant">Organic&nbsp;Hair Transplant</option>
              <option value="Organic&nbsp;Hair Treatment">Organic&nbsp;Hair Treatment</option>
              <option value="Simple&nbsp;Rhinoplasty">Simple&nbsp;Rhinoplasty</option>
              <option value="Otoplasty">Otoplasty</option>
              <option value="Incisionless Otoplasty">Incisionless Otoplasty</option>
              <option value="Breast Augmentation">Breast Augmentation</option>
              <option value="Breast Reduction">Breast Reduction</option>
              <option value="Breast Lifting">Breast Lifting</option>
              <option value="Face Lifting">Face Lifting</option>
              <option value="Blepharoplasty">Blepharoplasty</option>
              <option value="Lip Augmentation">Lip Augmentation</option>
              <option value="Lip&nbsp;Reduction">Lip&nbsp;Reduction</option>
              <option value="Eyebrow Lifting">Eyebrow Lifting</option>
              <option value="Neck Lifting">Neck Lifting</option>
              <option value="Cihantimur&nbsp;Fat Transfer">Cihantimur&nbsp;Fat Transfer</option>
              <option value="FUE Hair Transplant">FUE Hair Transplant</option>
              <option value="Bio Fiber Hair">Bio Fiber Hair</option>
              <option value="Eyebrow Transplent">Eyebrow Transplent</option>
              <option value="Moustache Transplent">Moustache Transplent</option>
              <option value="Prp Treatment">Prp Treatment</option>
              <option value="Hair Mesotherapy">Hair Mesotherapy</option>
              <option value="Beard Transplent">Beard Transplent</option>
              <option value="Scar Hair Transplent">Scar Hair Transplent</option>
              <option value="Gynecomastia">Gynecomastia</option>
              <option value="Genital Beautification">Genital Beautification</option>
              <option value="Arm Lifting">Arm Lifting</option>
              <option value="Fat Removal">Fat Removal</option>
              <option value="Epilation">Epilation</option>
              <option value="Weight Loss">Weight Loss</option>
              <option value="Ozone Treatment">Ozone Treatment</option>
              <option value="Tattoo Removal">Tattoo Removal</option>
              <option value="SkinDna Test">SkinDna Test</option>
              <option value="Other Treatment">Other Treatment</option>
            </select>
          </div>
            <div class="form-group">
              <select name="sendercountry" id="sendercountry" class="select form-control">              
                <option value="">Select Country</option>
                <option value="Germany">Germany</option>
                <option value="Afghanistan">Afghanistan</option>
                <option value="Albania">Albania</option>
                <option value="Algeria">Algeria</option>
                <option value="Andorra">Andorra</option>
                <option value="Angola">Angola</option>
                <option value="Antigua and Barbuda">Antigua and Barbuda</option>
                <option value="Argentina">Argentina</option>
                <option value="Armenia">Armenia</option>
                <option value="Australia">Australia</option>
                <option value="Austria">Austria</option>
                <option value="Azerbaijan">Azerbaijan</option>
                <option value="Bahamas">Bahamas</option>
                <option value="Bahrain">Bahrain</option>
                <option value="Bangladesh">Bangladesh</option>
                <option value="Barbados">Barbados</option>
                <option value="Belarus">Belarus</option>
                <option value="Belgium">Belgium</option>
                <option value="Belize">Belize</option>
                <option value="Benin">Benin</option>
                <option value="Bhutan">Bhutan</option>
                <option value="Bolivia">Bolivia</option>
                <option value="Bosnia and Herzegovina">Bosnia and Herzegovina</option>
                <option value="Botswana">Botswana</option>
                <option value="Brazil">Brazil</option>
                <option value="Brunei">Brunei</option>
                <option value="Bulgaria">Bulgaria</option>
                <option value="Burkina Faso">Burkina Faso</option>
                <option value="Burundi">Burundi</option>
                <option value="Cabo Verde">Cabo Verde</option>
                <option value="Cambodia">Cambodia</option>
                <option value="Cameroon">Cameroon</option>
                <option value="Canada">Canada</option>
                <option value="Central African Republic">Central African Republic</option>
                <option value="Chad">Chad</option>
                <option value="Chile">Chile</option>
                <option value="China">China</option>
                <option value="Colombia">Colombia</option>
                <option value="Comoros">Comoros</option>
                <option value="Congo, Republic of the">Congo, Republic of the</option>
                <option value="Congo, Democratic Republic of the">Congo, Democratic Republic of the</option>
                <option value="Costa Rica">Costa Rica</option>
                <option value="Cote d">Cote d</option>
                <option value="Croatia">Croatia</option>
                <option value="Cuba">Cuba</option>
                <option value="Cyprus">Cyprus</option>
                <option value="Czech Republic">Czech Republic</option>
                <option value="Denmark">Denmark</option>
                <option value="Djibouti">Djibouti</option>
                <option value="Dominica">Dominica</option>
                <option value="Dominican Republic">Dominican Republic</option>
                <option value="Ecuador">Ecuador</option>
                <option value="Egypt">Egypt</option>
                <option value="El Salvador">El Salvador</option>
                <option value="Equatorial Guinea">Equatorial Guinea</option>
                <option value="Eritrea">Eritrea</option>
                <option value="Estonia">Estonia</option>
                <option value="Ethiopia">Ethiopia</option>
                <option value="Fiji">Fiji</option>
                <option value="Finland">Finland</option>
                <option value="France">France</option>
                <option value="Gabon">Gabon</option>
                <option value="Gambia">Gambia</option>
                <option value="Georgia">Georgia</option>
                <option value="Germany">Germany</option>
                <option value="Ghana">Ghana</option>
                <option value="Greece">Greece</option>
                <option value="Grenada">Grenada</option>
                <option value="Guatemala">Guatemala</option>
                <option value="Guinea">Guinea</option>
                <option value="Guinea-Bissau">Guinea-Bissau</option>
                <option value="Guyana">Guyana</option>
                <option value="Haiti">Haiti</option>
                <option value="Honduras">Honduras</option>
                <option value="Hungary">Hungary</option>
                <option value="Iceland">Iceland</option>
                <option value="India">India</option>
                <option value="Indonesia">Indonesia</option>
                <option value="Iran">Iran</option>
                <option value="Iraq">Iraq</option>
                <option value="Ireland">Ireland</option>
                <option value="Israel">Israel</option>
                <option value="Italy">Italy</option>
                <option value="Jamaica">Jamaica</option>
                <option value="Japan">Japan</option>
                <option value="Jordan">Jordan</option>
                <option value="Kazakhstan">Kazakhstan</option>
                <option value="Kenya">Kenya</option>
                <option value="Kiribati">Kiribati</option>
                <option value="Kosovo">Kosovo</option>
                <option value="Kuwait">Kuwait</option>
                <option value="Kyrgyzstan">Kyrgyzstan</option>
                <option value="Laos">Laos</option>
                <option value="Latvia">Latvia</option>
                <option value="Lebanon">Lebanon</option>
                <option value="Lesotho">Lesotho</option>
                <option value="Liberia">Liberia</option>
                <option value="Libya">Libya</option>
                <option value="Liechtenstein">Liechtenstein</option>
                <option value="Lithuania">Lithuania</option>
                <option value="Luxembourg">Luxembourg</option>
                <option value="Macedonia">Macedonia</option>
                <option value="Madagascar">Madagascar</option>
                <option value="Malawi">Malawi</option>
                <option value="Malaysia">Malaysia</option>
                <option value="Maldives">Maldives</option>
                <option value="Mali">Mali</option>
                <option value="Malta">Malta</option>
                <option value="Marshall Islands">Marshall Islands</option>
                <option value="Mauritania">Mauritania</option>
                <option value="Mauritius">Mauritius</option>
                <option value="Mexico">Mexico</option>
                <option value="Micronesia">Micronesia</option>
                <option value="Moldova">Moldova</option>
                <option value="Monaco">Monaco</option>
                <option value="Mongolia">Mongolia</option>
                <option value="Montenegro">Montenegro</option>
                <option value="Morocco">Morocco</option>
                <option value="Mozambique">Mozambique</option>
                <option value="Namibia">Namibia</option>
                <option value="Nauru">Nauru</option>
                <option value="Nepal">Nepal</option>
                <option value="Netherlands">Netherlands</option>
                <option value="New Zealand">New Zealand</option>
                <option value="Nicaragua">Nicaragua</option>
                <option value="Niger">Niger</option>
                <option value="Nigeria">Nigeria</option>
                <option value="North Korea">North Korea</option>
                <option value="Norway">Norway</option>
                <option value="Oman">Oman</option>
                <option value="Pakistan">Pakistan</option>
                <option value="Palau">Palau</option>
                <option value="Palestine">Palestine</option>
                <option value="Panama">Panama</option>
                <option value="Papua New Guinea">Papua New Guinea</option>
                <option value="Paraguay">Paraguay</option>
                <option value="Peru">Peru</option>
                <option value="Philippines">Philippines</option>
                <option value="Poland">Poland</option>
                <option value="Portugal">Portugal</option>
                <option value="Qatar">Qatar</option>
                <option value="Romania">Romania</option>
                <option value="Russia">Russia</option>
                <option value="Rwanda">Rwanda</option>
                <option value="St. Kitts and Nevis">St. Kitts and Nevis</option>
                <option value="St. Lucia">St. Lucia</option>
                <option value="St. Vincent and The Grenadines">St. Vincent and The Grenadines</option>
                <option value="Samoa">Samoa</option>
                <option value="San Marino">San Marino</option>
                <option value="Sao Tome and Principe">Sao Tome and Principe</option>
                <option value="Saudi Arabia">Saudi Arabia</option>
                <option value="Senegal">Senegal</option>
                <option value="Serbia">Serbia</option>
                <option value="Seychelles">Seychelles</option>
                <option value="Sierra Leone">Sierra Leone</option>
                <option value="Singapore">Singapore</option>
                <option value="Slovakia">Slovakia</option>
                <option value="Slovenia">Slovenia</option>
                <option value="Solomon Islands">Solomon Islands</option>
                <option value="Somalia">Somalia</option>
                <option value="South Africa">South Africa</option>
                <option value="South Korea">South Korea</option>
                <option value="South Sudan">South Sudan</option>
                <option value="Spain">Spain</option>
                <option value="Sri Lanka">Sri Lanka</option>
                <option value="Sudan">Sudan</option>
                <option value="Suriname">Suriname</option>
                <option value="Swaziland">Swaziland</option>
                <option value="Sweden">Sweden</option>
                <option value="Switzerland">Switzerland</option>
                <option value="Syria">Syria</option>
                <option value="Taiwan">Taiwan</option>
                <option value="Tajikistan">Tajikistan</option>
                <option value="Tanzania">Tanzania</option>
                <option value="Thailand">Thailand</option>
                <option value="Timor-Leste">Timor-Leste</option>
                <option value="Togo">Togo</option>
                <option value="Tonga">Tonga</option>
                <option value="Trinidad and Tobago">Trinidad and Tobago</option>
                <option value="Tunisia">Tunisia</option>
                <option value="Turkey">Turkey</option>
                <option value="Turkmenistan">Turkmenistan</option>
                <option value="Tuvalu">Tuvalu</option>
                <option value="Uganda">Uganda</option>
                <option value="Ukraine">Ukraine</option>
                <option value="United Arab Emirates">United Arab Emirates</option>
                <option value="Uruguay">Uruguay</option>
                <option value="Uzbekistan">Uzbekistan</option>
                <option value="Vanuatu">Vanuatu</option>
                <option value="Venezuela">Venezuela</option>
                <option value="Vietnam">Vietnam</option>
                <option value="Yemen">Yemen</option>
                <option value="Zambia">Zambia</option>
                <option value="Zimbabwe">Zimbabwe</option>
                <option value="Other">Other</option>
    </select>
            </div>
            <input class="bordered" name="sendername" id="sendername" type="text" placeholder="Name" required>
            <div class="form-group">
            <select class="bordered form-control select" type="text" id="senderoperation" name="senderoperation" required="">
                                  <option value="">*Operation time</option>
                                  <option value="Hemen">Immediately</option> 
                                  <option value="Bu Ay İçinde">In this month</option> 
                                  <option value="Gelecek Ay içinde">In the next month</option>
                                  <option value="3 Ay İçinde">Within 3 months</option>
                                  <option value="6 ay içinde">Within 6 months</option>
                                  <option value="6 ay sonrası">After 6 months</option>
                    </select>
            </div>
            <input class="bordered" name="senderoperation" id="senderoperation" type="text" placeholder="What time are planning to do your operation">
            <input class="bordered" nmae="email" name="senderemail" id="senderemail" type="text" placeholder="Email">
            <input class="bordered" name="senderphone" id="senderphone" type="text" placeholder="Phone" required>
            <input class="bordered" name="sendermessage" id="sendermessage"  type="text" placeholder="Message">
            <input style="width:100%;" type="submit" value="SEND" class="btn-send block">
            </div>
            <br>
            <center>
            <h4 id="wait" style="display:none; color:white;">Please Wait&nbsp;<i class="feather feather-check-circle" data-feather="check-circle"></i></h4>
            </center>
            <h4 id="success" style="display:none; color:white;">Thank You, Message Successful</h4>
          </div>
        </form>
          <script>
             window.onload = function()
      {     
        $("#form_send").submit(function(e) {
          e.preventDefault();
          $(".btn-send input[type='submit']").attr("disabled", true);
          $('#wait').show();

              $.ajax({
                type: "POST",
                dataType: "text",
                url: '<?php bloginfo('template_url'); ?>/form/form.php',
                data: {
                    sendername: $('#sendername').val(),
                    senderphone: $('#senderphone').val(),
                    senderemail: $('#senderemail').val(),
                    senderoperation: $('#senderoperation').val(),
                    sendercountry: $('#sendercountry').val(),
                    sendermessage: $('#sendermessage').val(),
                    procedure: $('#procedure').val(),
                    page_link: "Web Form - "+ <?php echo $_SERVER['REQUEST_URI'];?>



                },
                success: function(data) {
                  $('#form').hide();
                  $('#wait').hide();
                  $('#success').show(300);
                },
            });

             });
      }

          </script>


