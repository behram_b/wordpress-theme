<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php the_content(); ?>
               

      <div class="comment-box">

              <?php 
              // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
              comments_template();
          endif;
              ?>

            </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>


<?php endwhile; else: ?>
<p><?php _e('Bulunamadı'); ?></p>
<?php endif; ?>