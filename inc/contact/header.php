<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<header class="main-header" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/bg/contact.jpg);">
    <div class="container-fluid">
        <nav class="navi-area">
            <div class="logo-area"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_option('est_logo'); ?>" alt="Logo"></a></div>
            <div class="right-area"><a href="#"><i class="fab fa-whatsapp" style="margin-right: 10px;"></i><span>+90</span><?php echo get_option('est_phone') ?></a><a class="btn-appointment" href="#">Free Appointment<i class="far fa-arrow-alt-circle-right"></i></a></div>
        </nav>
    </div>
    <?php get_template_part('inc/navbar_menus'); ?>
    <div class="container-fluid">
        <div class="hero-title is-dark">
            <h1><?php the_title(); ?></h1><!--            <b class="color-white">Aesthetic</b>-->
            <p><?php the_excerpt(); ?></p>
        </div>
        <!-- info buttons-->
        <ul class="detail-buttons is-listing">
            <li><a href="#">Ankara</a></li>
            <li><a href="#">Bursa (Inegol)</a></li>
            <li><a href="#">Bursa Çekirge</a></li>
            <li><a href="#">Bursa Korupark</a></li>
            <li><a href="#">Bursa Zafer Plaza</a></li>
            <li><a href="#">Istanbul Byomed</a></li>
            <li><a href="#">Izmir</a></li>
        </ul>
    </div>
</header>


<?php endwhile; else: ?>
    <p><?php _e('No found'); ?></p>
<?php endif; ?>