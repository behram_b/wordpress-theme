<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
    if ( !empty( get_the_content() ) ) {
        the_content();
    } else {
        echo '
 <div class="space_10 hidden-xs"></div>
    <section id="sub_block">
        <div class="main">
            <h4 class="text-center">The A-Team</h4>
            <h1 class="text-center">Estetik International <span>Team</span></h1>
            <h3>It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout.</h3>
        </div>
        <div class="space_25"></div>
    </section>
    <div class="space_10 hidden-xs"></div>
    <div class="sub_block_img cover-bg visible-xs" style="background: url(../wp-content/uploads/2018/02/doctors_content.jpg);"></div>
    <section class="col-xs-10 col-xs-push-1 col-md-push-1 col-md-10" id="sub_block_two">
        <div class="col-md-6 one">
            <h4>Plastic, Reconstructive and Aesthetics Surgeon</h4>
            <h1>Steven Lynch</h1>
            <div class="space_25"></div>
            <div class="divider"></div>
            <p>
                It is a long established fact that a reader will be distracted by the readable content of a page
                when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal
                distribution of letters, as opposed to using \'Content here, content here\', making it look like
                readable English.
            </p>
        </div>
        <div class="col-md-6"><a class="col-xs-6 col-md-4" href="#"><i class="fab fa-facebook-square" style="color: #adb5bd; font-size:3em;"></i>
                <h5>Facebook</h5></a><a class="col-xs-6 col-md-4" href="#"><i class="fab fa-instagram" style="color: #adb5bd; font-size:3em;"></i>
                <h5>Instagram</h5></a><a class="col-md-4 hidden-xs" href="#"><i class="far fa-arrow-alt-circle-right" style="color: #adb5bd; font-size:3em;"></i>
                <h5>More</h5></a></div>
        <div class="col-md-6 right_list">
            <div class="space_75 hidden-xs"></div>
            <div class="space_50 visible-xs"></div>
            <div class="divider"></div>
            <ul class="list-unstyled col-md-6">
                <li><i class="far fa-check-circle" style="font-size: 1.5em;"></i>                Specialist in Spider Web Technique</li>
                <li><i class="far fa-check-circle" style="font-size: 1.5em;"></i>                Total Face Rejuvenation</li>
                <li><i class="far fa-check-circle" style="font-size: 1.5em;"></i>                Genital Beautfication</li>
            </ul>
            <ul class="list-unstyled col-md-6">
                <li><i class="far fa-check-circle" style="font-size: 1.5em;"></i>                Simple Rhinoplasty</li>
                <li><i class="far fa-check-circle" style="font-size: 1.5em;"></i>                Cihantimur Fat Transfer</li>
            </ul>
        </div>
        <div class="ask_btn pull-right visible-xs"><a href="#"><i class="fas fa-comment" style="color: white; font-size: 2em;"></i></a>
            <h6>Ask Doctor</h6>
        </div>
    </section>
    <div class="sub_block_img cover-bg hidden-xs" style="background: url(../wp-content/uploads/2018/02/doctors_content.jpg);"></div>
    <div class="space_100"></div>
    <div class="sub_block_img cover-bg visible-xs" style="background: url(../wp-content/uploads/2018/02/doctors_content.jpg);"></div>
    <section class="col-md-push-1 col-md-10" id="sub_block_two">
        <div class="col-md-6 one">
            <h4>Plastic, Reconstructive and Aesthetics Surgeon</h4>
            <h1>Steven Lynch</h1>
            <div class="space_25"></div>
            <div class="divider"></div>
            <p>
                It is a long established fact that a reader will be distracted by the readable content of a page
                when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal
                distribution of letters, as opposed to using \'Content here, content here\', making it look like
                readable English.
            </p>
        </div>
        <div class="col-md-6"><a class="col-md-4" href="#"><i class="fab fa-facebook-square" style="color: #adb5bd; font-size:3em;"></i>
                <h5>stevenlynch</h5></a><a class="col-md-4" href="#"><i class="fab fa-instagram" style="color: #adb5bd; font-size:3em;"></i>
                <h5>stevenlynch</h5></a><a class="col-md-4 hidden-xs" href="#"><i class="far fa-arrow-alt-circle-right" style="color: #adb5bd; font-size:3em;"></i>
                <h5>More</h5></a></div>
        <div class="col-md-6 right_list">
            <div class="space_75 hidden-xs"></div>
            <div class="space_50 visible-xs"></div>
            <div class="divider"></div>
            <ul class="list-unstyled col-md-6">
                <li><i class="far fa-check-circle" style="font-size: 1.5em;"></i>                Specialist in Spider Web Technique</li>
                <li><i class="far fa-check-circle" style="font-size: 1.5em;"></i>                Total Face Rejuvenation</li>
                <li><i class="far fa-check-circle" style="font-size: 1.5em;"></i>                Genital Beautfication</li>
            </ul>
            <ul class="list-unstyled col-md-6">
                <li><i class="far fa-check-circle" style="font-size: 1.5em;"></i>                Simple Rhinoplasty</li>
                <li><i class="far fa-check-circle" style="font-size: 1.5em;"></i>                Cihantimur Fat Transfer</li>
            </ul>
        </div>
        <div class="ask_btn pull-right hidden-xs"><a href="#"><i class="fas fa-comment" style="color: white; font-size: 2em;"></i></a>
            <h6>Ask Doctor</h6>
        </div>
    </section>
    <div class="sub_block_img cover-bg hidden-xs" style="background: url(../wp-content/uploads/2018/02/doctors_content.jpg);"></div>
';
    }
?>


<?php endwhile; else: ?>
    <p><?php _e('Bulunamadı'); ?></p>
<?php endif; ?>
