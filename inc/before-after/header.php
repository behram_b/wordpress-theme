<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
 <header class="main-header mb has-gray-bg" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/bg/testimonials.jpg);">
      <div class="container-fluid">
        <nav class="navi-area">
          <div class="logo-area"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_option('est_logo'); ?>" alt="Logo"></a></div>
          <div class="right-area"><a href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo get_option('est_phone') ?>" style="margin-right: 10px;" alt=""><span>+90</span> 444 77 07</a><a class="btn-appointment" href="#">Free Appointment<i data-feather="arrow-right-circle"></i></a></div>
        </nav>
      </div>
      <?php get_template_part('inc/navbar_menus'); ?>
      <div class="container-fluid">
        <!-- hero title-->
        <div class="hero-title is-dark">
            <h1><?php the_title(); ?></h1><!--            <b class="color-white">Aesthetic</b>-->
            <p><?php the_excerpt(); ?></p>
          <!-- info buttons-->
        </div>
        <ul class="detail-buttons mt-60">
          <li><a href="#"><b>Online</b><small>Consultation</small></a></li>
          <li><a href="#"><b>Call Me</b><small>Back</small></a></li>
        </ul>
      </div>
    </header>
<?php endwhile; else: ?>
    <p><?php _e('No found'); ?></p>
<?php endif; ?>










