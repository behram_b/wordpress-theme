<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

        <div class="container-fluid">
      <div class="row topHeadingArea align-center flex">
        <div class="col-md-8 col-xs-12">
          <div class="top-title-area">
            <h4>Stories</h4><span>Drag slider bar left and right to view</span>
          </div>
        </div>
        <div class="col-md-2 col-xs-12">
          <div class="dropdown selectBox">
            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Select Category<span class="caret"></span></button>
            <ul class="dropdown-menu">
              <li><a href="#">Select Category</a></li>
              <li><a href="#">Select Category</a></li>
              <li><a href="#">Select Category</a></li>
            </ul>
          </div>
        </div>
        <div class="col-md-2 col-xs-12">
          <div class="dropdown selectBox">
            <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Select Country<span class="caret"></span></button>
            <ul class="dropdown-menu">
              <li><a href="#">Select Country</a></li>
              <li><a href="#">Select Country</a></li>
              <li><a href="#">Select Country</a></li>
            </ul>
          </div>
        </div>
      </div>
      
      <div class="container-fluid">
        <div class="row">

            <?php  
            $args=array(
            'cat' => 19,
            'orderby' => rand,
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 12,
            'caller_get_posts'=> 1,

            );
            $my_query = null;
            $my_query = new WP_Query($args);
            if( $my_query->have_posts() ) {
            while ($my_query->have_posts()) : $my_query->the_post(); ?>

         <?php $blog_id = get_option( 'page_for_posts' );
        $blog_image = MultiPostThumbnails::get_post_thumbnail_url( get_post_type(), 'secondary-image', $blog_id, 'full' );
               
        ?>     
          <div class="col-xs-12 col-md-3"><a class="thumbnail">
              <div class="icon_bf"></div>
              <div class="ba-slider"><img src="<?php the_post_thumbnail_url() ?>">
                <div class="resize"><img src="<?php echo $blog_image ?>"></div><span class="handle"></span>
              </div></a></div>
              <?php endwhile;   } wp_reset_query(); ?>
          <div class="col-xs-12 visible-xs text-center"><a class="button-1 btn-block" href="#" style="background: #17c9ad;min-height: 50px!important; border-radius: 50px">SEE ALL</a><br><br></div>
        </div>
      </div>
    </section>
          
      <div class="row">
        <div class="col-md-12 text-center"><a class="see-more" href="#">SEE MORE</a></div>
      </div>
    </div>

<?php endwhile; else: ?>
    <p><?php _e('No found'); ?></p>
<?php endif; ?>