<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <!-- page content--><br><br>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-9 col-md-8 col-xs-12">
                <div class="row"><a class="col-lg-4 col-md-4 col-xs-12 video-item"
                                    style="background: url(<?php bloginfo('template_url'); ?>/dist/img/video-bg.jpg);"
                                    href="#">
                        <div class="play_item"><i class="fa fa-play"></i></div>
                        <span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır.</span></span></a><a
                            class="col-lg-4 col-md-4 col-xs-12 video-item"
                            style="background: url(<?php bloginfo('template_url'); ?>/dist/img/video-bg-2.jpg);"
                            href="#">
                        <div class="play_item"><i class="fa fa-play"></i></div>
                        <span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır.</span></span></a><a
                            class="col-lg-4 col-md-4 col-xs-12 video-item"
                            style="background: url(<?php bloginfo('template_url'); ?>/dist/img/video-bg-3.jpg);"
                            href="#">
                        <div class="play_item"><i class="fa fa-play"></i></div>
                        <span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır.</span></span></a>
                    <div class="clearfix"></div>
                    <?php
                    if (!empty(get_the_content())) {
                        the_content();
                    } else {
                        ?>
                        
        <div class="col-md-12 post-desc-area">
    <h4>Organik Saç Ekimi Nedir?</h4>
    <p class="lead justify">Saç Ekimi sonrası Kalıcı güçlü ve daha gür saçlar için Organik Saç Ekimi tercih edebilirsiniz. Organik Saç Ekimindeki temel amaç nakledilen ve ekilen saçların tutunabilmesi ve sağlıklı şekilde beslenebilmesi için ilk önce saç derisini güçlendirmektir.Böylece ekilecek olan saçlar tutunabilir, beslenebilir ve sağlıkla uzun yıllar size eşlik eder.</p>
    <img class="full-width" src="../wp-content/uploads/2018/02/post-item-300x167.jpg" alt="" />
    </div>
    <div class="col-md-12 post-desc-video-area" style="background: url(\'../wp-content/uploads/2018/02/post-item-2.jpg\');">
    <h3>Kök Hücre ile
        Saç Ekimi Nedir?
    </h3>
    <div class="play_item"></div>
    <span class="color-white">WATCH VIDEO</span>
    </div>
    <div class="col-md-12 post-desc-area">
    <h4>Organik Saç Ekimi Nasıl Yapılır?</h4>
    <p class="lead justify">Hastanın kendi yağ dokusundan alınan ve kök hücreler ile zenginleştirilen sıvı, saç ekimi yapılacak bölgeye enjekte edilir. Bu sayede hem çalışma alanı şişirilerek rahat nakil ortamı sağlanır, hem de klasik saç ekimi işlemlerinde saç köklerini zedeleyen kimyasal enjeksiyona gerek kalmaz. Uygulama için hazır ve verimli hale getirilen saçtan yoksun bölgeye donör alandan alınan sağlıklı kıl kökleri uzman cerrahımız tarafından kolaylıkla nakledilir.</p>
    </div>
    <div class="col-md-12 bg-item-area" style="background: url(\'../wp-content/uploads/2018/02/post-item-3.jpg\');">
    <h3>Neden Organik Saç Ekimi?</h3>
    <p class="lead">Saç Ekimi sonrası Kalıcı güçlü ve daha gür saçlar için Organik Saç Ekimi tercih edebilirsiniz. Organik Saç Ekimindeki temel amaç nakledilen ve ekilen saçların tutunabilmesi ve sağlıklı şekilde beslenebilmesi için ilk önce saç derisini güçlendirmektir. Böylece ekilecek olan saçlar tutunabilir, beslenebilir ve sağlıkla uzun yıllar size eşlik eder.</p>
    </div>
    <div class="col-md-12 comment-area">
    <h5>Sıkça Sorulan<b> Sorular</b></h5>
    <div id="accordion" class="panel-group" role="tablist" aria-multiselectable="true">
        <div class="panel panel-default">
            <div id="headingOne" class="panel-heading" role="tab">
                <h4 class="panel-title"><a role="button" href="#collapseOne" data-toggle="collapse" data-parent="#accordion" aria-expanded="true" aria-controls="collapseOne">Why should I have Spider Web Aesthetics?</a></h4>
            </div>
            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.</div>
            </div>
        </div>
        <div class="panel panel-default">
            <div id="headingTwo" class="panel-heading" role="tab">
                <h4 class="panel-title"><a class="collapsed" role="button" href="#collapseTwo" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseTwo">Recovery and Results</a></h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.</div>
            </div>
        </div>
        <div class="panel panel-default">
            <div id="headingThree" class="panel-heading" role="tab">
                <h4 class="panel-title"><a class="collapsed" role="button" href="#collapseThree" data-toggle="collapse" data-parent="#accordion" aria-expanded="false" aria-controls="collapseThree">Advantages</a></h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven\'t heard of them accusamus labore sustainable VHS.</div>
            </div>
        </div>
    </div>
    </div>
    <!--more--> <!--Sayfanın özeti bu etiketle alınır--> 

                        
                  <?php } ?>

                    ?>

                    <?php comments_template('/comments.php') ?>
                        <h5>Yorum <b>Yapın</b></h5>
                        <div class="comment-box">
                            <div class="user">
                                <div><img src="<?php bloginfo('template_url'); ?>/dist/img/user-2.png" alt=""></div>
                                <span class="stars"><small>OYUNUZ</small><i class="fa fa-star"></i><i
                                            class="fa fa-star"></i><i class="fa fa-star"></i><i
                                            class="fa fa-star gray"></i><i class="fa fa-star gray"></i></span>
                            </div>
                            <textarea class="form-control" type="text" placeholder="Yorumunuz.."></textarea>
                            <button class="btn-send pull-right">GÖNDER</button>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-4 col-xs-12">
                <div class="contact-left is-small cover-bg"
                     style="background: url(<?php bloginfo('template_url'); ?>/dist/img/bg/contact-form-2.jpg);">
                    <h6>Call Me Back</h6>
                    <select class="form-control dark">
                        <option>Select Treatment</option>
                        <option>Select Treatment</option>
                        <option>Select Treatment</option>
                    </select>
                    <select class="form-control dark">
                        <option>Select Country</option>
                        <option>Select Country</option>
                        <option>Select Country</option>
                    </select>
                    <input class="bordered" type="text" placeholder="Name">
                    <input class="bordered" type="text" placeholder="What time are planning to do your operation">
                    <input class="bordered" type="text" placeholder="Email">
                    <input class="bordered" type="text" placeholder="Phone">
                    <input class="bordered" type="text" placeholder="Message">
                    <button class="btn-send pull-right">SEND</button>
                </div>
                <div class="col-md-12"><a class="cover-bg img-link" href="#"
                                          style="background: url(<?php bloginfo('template_url'); ?>/dist/img/block3/block3_two_background.jpg);"><span
                                class="title">Spider Web Aesthetic</span><span class="desc">Örümcek Ağı EstetiğiÖrümcek Ağı Estetiği yaşlanma belirtilerinin önüne geçmenize yardımcı, ameliyatsız bir gençleşme</span></a><a
                            class="cover-bg img-link" href="#"
                            style="background: url(<?php bloginfo('template_url'); ?>/dist/img/block3/block3_three_background.jpg);"><span
                                class="title">Simple Rhinoplasty</span><span class="desc">Basit burun estetiği, neştersiz burun estetiği olarak da geçen son teknolojik bir uygulamadır. Burnun insanın ifadesini ve </span></a>
                    <div class="subscribeBox">
                        <h4>E-Mail Subscription</h4><span>Sign up for our plastic surgery newsletter</span>
                        <div class="flex end">
                            <input type="text" placeholder="Your e-mail address">
                            <button>OK</button>
                        </div>
                    </div>
                    <br><br>
                    <div class="subscribeBox">
                        <h4>2017 DECEMBER EI MAG</h4><a href="#"><img
                                    src="<?php bloginfo('template_url'); ?>/dist/img/pages/magazine.jpg" alt=""></a>
                    </div>
                </div>
            </div>
        </div>
    </div>



<?php endwhile; else: ?>
    <p><?php _e('Bulunamadı'); ?></p>
<?php endif; ?>


