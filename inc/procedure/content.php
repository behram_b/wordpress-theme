<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

  <?php
                    if (!empty(get_the_content())) {
                        the_content();
                    } else {
                        ?>

<div class="container-fluid">
      <div class="row">
        <div class="col-lg-9 col-md-8 col-xs-12">
          <div class="row">
            <div class="col-lg-4 col-md-4 col-xs-12"><a class="video-item" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/video-bg.jpg);" href="#">
                <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır.</span></span></a></div>
            <div class="col-lg-4 col-md-4 col-xs-12"><a class="video-item" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/video-bg-2.jpg);" href="#">
                <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır.</span></span></a></div>
            <div class="col-lg-4 col-md-4 col-xs-12"><a class="video-item" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/video-bg-3.jpg);" href="#">
                <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır.</span></span></a></div>
            <div class="clearfix"></div>
            <div class="col-md-12">
              <div class="white-bg-description">
                <h2>Total Face Rejuvenation</h2>
                <p>Our face where our expressions of most feelings become visible is one of the most complex part of our body composing several different kind of tissues and functions. Asza result of this complexity the one-side solutions for aged faces condemned to fail and to obtain more satisfied results in harmony challenges should be considered as a whole like in natural aging process. In this aim we apply total face rejuvenation including face, eyebrow and neck lifting, upper and lower blepharoplasty, stem cell enriched fat transfer and Spider Web Aesthetics which gives us excellent results turning effects of time back on behalf of you.</p>
              </div>
            </div>
            <div class="col-md-12">
              <div class="video-item-description cover-bg" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/video-prew.jpg);">
                <div class="play_item ml-0"><i data-feather="play"></i></div>
                <div class="row">
                  <h3 class="col-md-6 col-xs-12">Kök Hücre ile Saç Ekimi Nedir?</h3>
                  <div class="clearfix"></div>
                  <p class="col-md-6 col-xs-12">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır. Ekim öncesinde saç derisine enjekte edilen sıvıda bol miktarda kök hücre bulunur.</p>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="blue-desc-item">
                <div class="row">
                  <div class="col-md-12">
                    <h4>Why should I have Organic Hair Transplantation?</h4>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <p>Stem cells are one of the most popular topics in the science area and it is a well-known fact that the life was built on the shoulders of these cells. Stem cells are used in treatment of many diseases today. Aesthetic plastic surgery is another area that stem cells are used for their advantages. Stem cells are obtained from the own adipose tissue of the patient. It can be stated that stem cells transferred to another place in the body yields rejuvenating, refreshing and repairing effects on the tissue. “Organic Hair Transplantation” method uses the advantages of the stem cells at the maximum level. <br><br>If we consider the recipient area as an empty field and the grafts as a seed; the relationship between a good quality recipient area and the grafts can be seen</p>
                  </div>
                  <div class="col-md-6 col-xs-12">
                    <p>cells transferred to another place in the body yields rejuvenating, refreshing and repairing effects on the tissue. “Organic Hair Transplantation” method uses the advantages of the stem cells at the maximum level. <br><br>If we consider the recipient area as an empty field and the grafts as a seed; the relationship between a good quality recipient area and the grafts can be seen obviously since the seeds can grow better on a healthy field. Therefore the field, meaning the recipient area, is need to be refreshed prior to the transplantation process. Loss of hair is an independent predictor of the poor quality of the scalp. The stem cells help to rejuvenate the recipient area in order to obtain a better quality scalp.</p>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12">
              <div class="orange-item cover-bg" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/orange-bg.jpg);"><span>Organic Hair Transplantation</span>
                <h5>How the <b>“Organic Hair Transplantation”</b> is performed?</h5>
                <p>Fat tissue obtained from own body of the patient is processed and enriched from adipose tissue derived stem cells with the Cihantimur Fat Transfer Technique which uses closed circuit system. The recipient area that the “Organic Hair Transplantation” will be performed is sterilized and the mixture containing adipose tissue derived stem cells is injected to the area. The volume of the scalp is increased and nutrition process begins. Meanwhile hair grafts are extracted from the donor site with use of micro bistoury.</p>
                <p>The extracted hair grafts then classified according to their types and prepared for the transplantation process. In order to get the most natural result the angles of the hair canals are determined precisely. The donor site is dressed after the “Organic Hair Transplantation” process is completed which will be removed the following day. The recipient area is left as open.</p>
                <h6>Differences between “Organic Hair Transplantation” and classical hair transplantation :</h6>
                <p>Fat tissue obtained from own body of the patient is processed and enriched from adipose tissue derived stem cells with the Cihantimur Fat Transfer Technique which uses closed circuit system. The recipient area that the “Organic Hair Transplantation” will be performed is sterilized and the mixture containing adipose tissue derived stem cells is injected to the area. The volume of the scalp is increased and nutrition process begins. Meanwhile hair grafts are extracted from the donor site with use of micro bistoury.</p>
              </div>
            </div>
            <div class="col-md-12">
              <div class="white-bg-description">
                <h2>Total Face Rejuvenation</h2>
                <p>Our face where our expressions of most feelings become visible is one of the most complex part of our body composing several different kind of tissues and functions. Asza result of this complexity the one-side solutions for aged faces condemned to fail and to obtain more satisfied results in harmony challenges should be considered as a whole like in natural aging process. In this aim we apply total face rejuvenation including face, eyebrow and neck lifting, upper and lower blepharoplasty, stem cell enriched fat transfer and Spider Web Aesthetics which gives us excellent results turning effects of time back on behalf of you.</p>
              </div>
            </div>
            <div class="col-md-12">
              <div class="row">
                <div class="col-md-12 text-center">
                  <h2 class="main--title"><span>OUR</span> VIDEOS</h2>
                </div>
                <div class="col-lg-4 col-md-4 col-xs-12"><a class="video-item hoverable-item" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/video-bg.jpg);" href="#">
                    <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır.</span></span></a></div>
                <div class="col-lg-4 col-md-4 col-xs-12"><a class="video-item hoverable-item" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/video-bg-2.jpg);" href="#">
                    <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır.</span></span></a></div>
                <div class="col-lg-4 col-md-4 col-xs-12"><a class="video-item hoverable-item" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/video-bg-3.jpg);" href="#">
                    <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır. </span></span></a></div>
                <div class="col-lg-4 col-md-4 col-xs-12"><a class="video-item hoverable-item" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/video-bg.jpg);" href="#">
                    <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır.</span></span></a></div>
                <div class="col-lg-4 col-md-4 col-xs-12"><a class="video-item hoverable-item" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/video-bg-2.jpg);" href="#">
                    <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır.</span></span></a></div>
                <div class="col-lg-4 col-md-4 col-xs-12"><a class="video-item hoverable-item" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/video-bg-3.jpg);" href="#">
                    <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır.</span></span></a></div>
              </div>
            </div>
            <div class="col-md-12 comment-area">
              <h5>Sıkça Sorulan<b> Sorular</b></h5>
              <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                  <div class="panel-heading" id="headingOne" role="tab">
                    <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Why should I have Spider Web Aesthetics?</a></h4>
                  </div>
                  <div class="panel-collapse collapse in" id="collapseOne" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading" id="headingTwo" role="tab">
                    <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">Recovery and Results</a></h4>
                  </div>
                  <div class="panel-collapse collapse" id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo">
                    <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                  </div>
                </div>
                <div class="panel panel-default">
                  <div class="panel-heading" id="headingThree" role="tab">
                    <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">Advantages</a></h4>
                  </div>
                  <div class="panel-collapse collapse" id="collapseThree" role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.</div>
                  </div>
                </div>
              </div>
              <h5 class="comment-title">Kimler Neler<b> Demiş?</b><a class="btn-send pull-right radius" href="#">YORUM YAZ</a></h5>
                            <div class="comment-box">
                              <meta itemprop="datePublished" content="20-02-2018">
                              <div class="user">
                                <div><img src="<?php bloginfo('template_url'); ?>/dist/img/user-2.png" alt=""><span class="name" itemprop="name">Philip Drew</span></div><span class="stars" itemprop="ratingValue"><i data-feather="star" itemprop="bestRating"></i><i data-feather="star" itemprop="bestRating"></i><i data-feather="star" itemprop="bestRating"></i><i class="gray" data-feather="star" itemprop="bestRating"></i><i class="gray" data-feather="star" itemprop="bestRating"></i></span>
                              </div>
                              <p itemprop="description">There is nothing I would change about The medical Group or their services. I am extremely happy with my results and would whole heart recom...</p>
                            </div>
                            <div class="comment-box">
                              <meta itemprop="datePublished" content="20-02-2018">
                              <div class="user">
                                <div><img src="<?php bloginfo('template_url'); ?>/dist/img/user-2.png" alt=""><span class="name" itemprop="name">Philip Drew</span></div><span class="stars" itemprop="ratingValue"><i data-feather="star" itemprop="bestRating"></i><i data-feather="star" itemprop="bestRating"></i><i data-feather="star" itemprop="bestRating"></i><i class="gray" data-feather="star" itemprop="bestRating"></i><i class="gray" data-feather="star" itemprop="bestRating"></i></span>
                              </div>
                              <p itemprop="description">There is nothing I would change about The medical Group or their services. I am extremely happy with my results and would whole heart recom...</p>
                            </div>
                            <div class="comment-box">
                              <meta itemprop="datePublished" content="20-02-2018">
                              <div class="user">
                                <div><img src="<?php bloginfo('template_url'); ?>/dist/img/user-2.png" alt=""><span class="name" itemprop="name">Philip Drew</span></div><span class="stars" itemprop="ratingValue"><i data-feather="star" itemprop="bestRating"></i><i data-feather="star" itemprop="bestRating"></i><i data-feather="star" itemprop="bestRating"></i><i class="gray" data-feather="star" itemprop="bestRating"></i><i class="gray" data-feather="star" itemprop="bestRating"></i></span>
                              </div>
                              <p itemprop="description">There is nothing I would change about The medical Group or their services. I am extremely happy with my results and would whole heart recom...</p>
                            </div>
              <h5>Yorum <b>Yapın</b></h5>
              <div class="comment-box">
                <div class="user">
                  <div><img src="img/user-2.png" alt=""></div><span class="stars"><small>OYUNUZ</small><i data-feather="star" itemprop="bestRating"></i><i data-feather="star" itemprop="bestRating"></i><i data-feather="star" itemprop="bestRating"></i><i class="grayi" data-feather="star" itemprop="bestRating"></i><i class="grayi" data-feather="star" itemprop="bestRating"></i></span>
                </div>
                <textarea class="form-control" type="text" placeholder="Yorumunuz.."></textarea>
                <button class="btn-send pull-right radius">GÖNDER</button>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-lg-3 col-md-4 col-xs-12">
          <div class="contact-left is-small cover-bg" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/bg/contact-form-3.jpg);">
            <h6>Call Me Back<i data-feather="arrow-down-circle"></i></h6>
            <div class="dropdown selectBox">
              <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Select Treatment<span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="#">Select Treatment</a></li>
                <li><a href="#">Select Treatment</a></li>
                <li><a href="#">Select Treatment</a></li>
              </ul>
            </div>
            <div class="dropdown selectBox">
              <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">Select Country<span class="caret"></span></button>
              <ul class="dropdown-menu">
                <li><a href="#">Select Country</a></li>
                <li><a href="#">Select Country</a></li>
                <li><a href="#">Select Country</a></li>
              </ul>
            </div>
            <input class="bordered" type="text" placeholder="Name">
            <input class="bordered" type="text" placeholder="What time are planning to do your operation">
            <input class="bordered" type="text" placeholder="Email">
            <input class="bordered" type="text" placeholder="Phone">
            <input class="bordered" type="text" placeholder="Message">
            <button class="btn-send block">SEND</button>
          </div>
          <div class="col-md-12"><a class="cover-bg img-link" href="#" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/block3/block3_two_background.jpg);"><span class="title">Spider Web Aesthetic</span><span class="desc">Örümcek Ağı EstetiğiÖrümcek Ağı Estetiği yaşlanma belirtilerinin önüne geçmenize yardımcı, ameliyatsız bir gençleşme</span></a><a class="cover-bg img-link" href="#" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/block3/block3_three_background.jpg);"><span class="title">Simple Rhinoplasty</span><span class="desc">Basit burun estetiği, neştersiz burun estetiği olarak da geçen son teknolojik bir uygulamadır. Burnun insanın ifadesini ve </span></a>
            <div class="subscribeBox">
              <h4>E-Mail Subscription</h4><span>Sign up for our plastic surgery newsletter</span>
              <div class="flex end">
                <input type="text" placeholder="Your e-mail address">
                <button>OK</button>
              </div>
            </div><br><br>
            <div class="subscribeBox">
              <h4>2017 DECEMBER EI MAG</h4><a href="#"><img src="<?php bloginfo('template_url'); ?>/dist/img/pages/magazine.jpg" alt=""></a>
            </div>
          </div>
        </div>
      </div>
    </div>
  
        <?php } ?>

    <?php endwhile; else: ?>
    <p><?php _e('Bulunamadı'); ?></p>
<?php endif; ?>
