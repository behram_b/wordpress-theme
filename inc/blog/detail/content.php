<div class="container-fluid">
      <div class="test-item-cont"><img src="./img/bg/testimonial-item.jpg" alt="">
        <div class="white-info-area"><span class="date">29<b>JUN</b></span>
          <h2>Erkeklerin Estetik Kılavuzu</h2>
          <p>Estetik cerrahi ve medikal uygulamalar sadece kadınların tekelindeymiş ve çoğunlukla kadınlar bu uygulamalardan yardım alıyormuş gibi gözükse de, son yıllarda değişim yaşanıyor. Artık erkekler de bakımlarına ve estetik duruşlarına son derece önem veriyorlar</p>
        </div>
        <div class="desc news-desc text-left">
          <div class="social-item-area"><span>SHARE:</span>
            <ul>
              <li><a href="#"><i data-feather="twitter"></i></a></li>
              <li><a href="#"><i data-feather="instagram"></i></a></li>
              <li><a href="#"><i data-feather="facebook"></i></a></li>
            </ul>
          </div>
          <p>Ülkemizde erkeklerin estetik cerrahiye artan taleplerini ve en fazla hangi prosedürü talep ettiklerini Estetik Plastik ve Rekonstrüktif Cerrahi Uzmanı Op. Dr. Bülent Cihantimur anlattı: “ Ülkemizde erkeklerin estetiğe ilgisi hızlı bir şekilde artıyor. Sağlık turizmi ile gelen yabancı erkeklerin sayısı da aynı ölçüde artış gösteriyor. Bunun en büyük nedeni bana göre kadınlar. Çünkü kadınlar artık son derece bakımlı, estetik duruşlarından kesinlikle taviz vermiyorlar. Kadınlar hem eşlerini teşvik ediyor, hem de erkekler bu denli bakımlı eşlerinin yanında kendilerini de iyi hissetmek istiyorlar. İkinci neden ise, tabii ki iş hayatı… Artık herkes eğitimli, herkes kalifiye, bir üst levela geçebilmek iyi görüntüyle doğru orantılı. İş dünyası taze, dinamik, genç ve fit bir çalışan veya patron görmek istiyor. Erkek hastalarımızın mesleklerine bakarsak, mali müşavirlerden, avukatlara, emlakçılardan profesyonel sporculara kadar geniş bir yelpazede bizden yardım alan bireyler olduğunu görüyoruz.</p>
          <p>Kliniklerimizde sadece erkek hastalarımız için tasarlanmış, geniş bir hizmet yelpazesi sunuyoruz. Bunların içinde cerrahi yaklaşımlar, lazer ve ışık tedavileri, saç ekimi, cilt gençleştirme tedavileri gibi pek çok seçenek bulunuyor. Erkeklere en fazla uyguladığımız operasyonlara bakacak olursak, birinci sırada saç ekimini görüyoruz. Karın kası estetiği veya vücut şekillendirme operasyonları, jinekomasti, liposuction, erkek cinsel organ estetiği(penis büyütme), göz çevresi estetiği gibi uygulamalar yine en fazla talep edilen prosedürlerdir.</p>
          <h6 class="default">Erkeklerde meme büyüklüğü-jinekomasti</h6>
          <p>Jinekomasti ameliyatları, erkeklerde meme dokusunun büyümesiyle oluşan estetik sorunun çözümlenmesine fayda sağlayan operasyonlardır. Gelişen teknolojiyle birlikte artık son derece konforlu jinekomasti operasyonları yapabiliyoruz. Kendi geliştirdiği teknikle kesi yapılmadan, sadece mikro deliklerle yağlı dokuya ulaşıyoruz. Yağ dokusunu çözen bu yöntem ayrıca işlemi yaparken de kesinlikle sinirlere ya da damarlara zarar vermiyor. Bu şekilde işlem tamamlandıktan sonra minimal şişlik ya da morlukla karşılaşılır ve ya hiç gözlemlenmez.</p>
        </div>
      </div>
    </div>
    <div class="container-fluid">
      <div class="title--area">
        <h5>OTHER<span> BLOGS</span></h5>
        <div class="two-items-slider slider-area">
          <article class="slider-item is-flex-item"><a style="background: url(<?php bloginfo('template_url'); ?>/dist/img/bg/testimonial-item.jpg);"><span class="play_item"><i data-feather="play"></i></span></a>
            <div class="desc"><span>Estetik International is awarded the WhatClinic.com 2014</span><small>Estetik International – Istanbul was awarded a top industry customer service award this week, based on feedback from </small>
              <div class="down-area"><a class="read-more" href="#"><i data-feather="arrow-right-circle"></i>Read More</a><span class="date">22 Feb 2018 </span></div>
            </div>
          </article>
          <article class="slider-item is-flex-item"><a style="background: url(<?php bloginfo('template_url'); ?>/dist/img/bg/testimonial-item.jpg);"><span class="play_item"><i data-feather="play"></i></span></a>
            <div class="desc"><span>Estetik International is awarded the WhatClinic.com 2014</span><small>Estetik International – Istanbul was awarded a top industry customer service award this week, based on feedback from </small>
              <div class="down-area"><a class="read-more" href="#"><i data-feather="arrow-right-circle"></i>Read More</a><span class="date">22 Feb 2018 </span></div>
            </div>
          </article>
          <article class="slider-item is-flex-item"><a style="background: url(<?php bloginfo('template_url'); ?>/dist/img/bg/testimonial-item.jpg);"><span class="play_item"><i data-feather="play"></i></span></a>
            <div class="desc"><span>Estetik International is awarded the WhatClinic.com 2014</span><small>Estetik International – Istanbul was awarded a top industry customer service award this week, based on feedback from </small>
              <div class="down-area"><a class="read-more" href="#"><i data-feather="arrow-right-circle"></i>Read More</a><span class="date">22 Feb 2018 </span></div>
            </div>
          </article>
          <article class="slider-item is-flex-item"><a style="background: url(<?php bloginfo('template_url'); ?>/dist/img/bg/testimonial-item.jpg);"><span class="play_item"><i data-feather="play"></i></span></a>
            <div class="desc"><span>Estetik International is awarded the WhatClinic.com 2014</span><small>Estetik International – Istanbul was awarded a top industry customer service award this week, based on feedback from </small>
              <div class="down-area"><a class="read-more" href="#"><i data-feather="arrow-right-circle"></i>Read More</a><span class="date">22 Feb 2018 </span></div>
            </div>
          </article>
          <article class="slider-item is-flex-item"><a style="background: url(<?php bloginfo('template_url'); ?>/dist/img/bg/testimonial-item.jpg);"><span class="play_item"><i data-feather="play"></i></span></a>
            <div class="desc"><span>Estetik International is awarded the WhatClinic.com 2014</span><small>Estetik International – Istanbul was awarded a top industry customer service award this week, based on feedback from </small>
              <div class="down-area"><a class="read-more" href="#"><i data-feather="arrow-right-circle"></i>Read More</a><span class="date">22 Feb 2018 </span></div>
            </div>
          </article>
          <article class="slider-item is-flex-item"><a style="background: url(<?php bloginfo('template_url'); ?>/dist/img/bg/testimonial-item.jpg);"><span class="play_item"><i data-feather="play"></i></span></a>
            <div class="desc"><span>Estetik International is awarded the WhatClinic.com 2014</span><small>Estetik International – Istanbul was awarded a top industry customer service award this week, based on feedback from </small>
              <div class="down-area"><a class="read-more" href="#"><i data-feather="arrow-right-circle"></i>Read More</a><span class="date">22 Feb 2018 </span></div>
            </div>
          </article>
          <article class="slider-item is-flex-item"><a style="background: url(<?php bloginfo('template_url'); ?>/dist/img/bg/testimonial-item.jpg);"><span class="play_item"><i data-feather="play"></i></span></a>
            <div class="desc"><span>Estetik International is awarded the WhatClinic.com 2014</span><small>Estetik International – Istanbul was awarded a top industry customer service award this week, based on feedback from </small>
              <div class="down-area"><a class="read-more" href="#"><i data-feather="arrow-right-circle"></i>Read More</a><span class="date">22 Feb 2018 </span></div>
            </div>
          </article>
          <article class="slider-item is-flex-item"><a style="background: url(<?php bloginfo('template_url'); ?>/dist/img/bg/testimonial-item.jpg);"><span class="play_item"><i data-feather="play"></i></span></a>
            <div class="desc"><span>Estetik International is awarded the WhatClinic.com 2014</span><small>Estetik International – Istanbul was awarded a top industry customer service award this week, based on feedback from </small>
              <div class="down-area"><a class="read-more" href="#"><i data-feather="arrow-right-circle"></i>Read More</a><span class="date">22 Feb 2018 </span></div>
            </div>
          </article>
        </div>
      </div>
    </div>