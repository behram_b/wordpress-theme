
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
if ( !empty( get_the_content() ) ) {
    the_content();
} else {

    ?>
<div class="container-fluid">
            <div class="testimonial-box is-blog-item is-white-item row flex"><a class="col-md-5 col-xs-12 bg-item" href="#" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/banners/testimonial-1.jpg);"><span class="play-icon"><i data-feather="play"></i></span></a>
              <div class="col-md-7 col-xs-12 info-area"><span class="date is-pink">29 <b>JUN</b></span>
                <h2>Erkeklerin Estetik Kılavuzu</h2>
                <p>Estetik cerrahi ve medikal uygulamalar sadece kadınların tekelindeymiş ve çoğunlukla kadınlar bu uygulamalardan yardım alıyormuş gibi gözükse de, son yıllarda değişim yaşanıyor. Artık erkekler de bakımlarına ve estetik duruşlarına son derece önem veriyorlar</p>
                <p>Ülkemizde erkeklerin estetik cerrahiye artan taleplerini ve en fazla hangi prosedürü talep ettiklerini Estetik Plastik ve Rekonstrüktif Cerrahi Uzmanı Op. Dr. Bülent Cihantimur anlattı:</p><a class="right-btn" href="#"><i data-feather="arrow-right-circle"></i></a>
              </div>
            </div>
            <div class="testimonial-box is-blog-item is-white-item row flex"><a class="col-md-5 col-xs-12 bg-item" href="#" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/banners/testimonial-1.jpg);"><span class="play-icon"><i data-feather="play"></i></span></a>
              <div class="col-md-7 col-xs-12 info-area"><span class="date is-pink">29 <b>JUN</b></span>
                <h2>Erkeklerin Estetik Kılavuzu</h2>
                <p>Estetik cerrahi ve medikal uygulamalar sadece kadınların tekelindeymiş ve çoğunlukla kadınlar bu uygulamalardan yardım alıyormuş gibi gözükse de, son yıllarda değişim yaşanıyor. Artık erkekler de bakımlarına ve estetik duruşlarına son derece önem veriyorlar</p>
                <p>Ülkemizde erkeklerin estetik cerrahiye artan taleplerini ve en fazla hangi prosedürü talep ettiklerini Estetik Plastik ve Rekonstrüktif Cerrahi Uzmanı Op. Dr. Bülent Cihantimur anlattı:</p><a class="right-btn" href="#"><i data-feather="arrow-right-circle"></i></a>
              </div>
            </div>
            <div class="testimonial-box is-blog-item is-white-item row flex"><a class="col-md-5 col-xs-12 bg-item" href="#" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/banners/testimonial-1.jpg);"><span class="play-icon"><i data-feather="play"></i></span></a>
              <div class="col-md-7 col-xs-12 info-area"><span class="date is-pink">29 <b>JUN</b></span>
                <h2>Erkeklerin Estetik Kılavuzu</h2>
                <p>Estetik cerrahi ve medikal uygulamalar sadece kadınların tekelindeymiş ve çoğunlukla kadınlar bu uygulamalardan yardım alıyormuş gibi gözükse de, son yıllarda değişim yaşanıyor. Artık erkekler de bakımlarına ve estetik duruşlarına son derece önem veriyorlar</p>
                <p>Ülkemizde erkeklerin estetik cerrahiye artan taleplerini ve en fazla hangi prosedürü talep ettiklerini Estetik Plastik ve Rekonstrüktif Cerrahi Uzmanı Op. Dr. Bülent Cihantimur anlattı:</p><a class="right-btn" href="#"><i data-feather="arrow-right-circle"></i></a>
              </div>
            </div>
            <div class="testimonial-box is-blog-item is-white-item row flex"><a class="col-md-5 col-xs-12 bg-item" href="#" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/banners/testimonial-1.jpg);"><span class="play-icon"><i data-feather="play"></i></span></a>
              <div class="col-md-7 col-xs-12 info-area"><span class="date is-pink">29 <b>JUN</b></span>
                <h2>Erkeklerin Estetik Kılavuzu</h2>
                <p>Estetik cerrahi ve medikal uygulamalar sadece kadınların tekelindeymiş ve çoğunlukla kadınlar bu uygulamalardan yardım alıyormuş gibi gözükse de, son yıllarda değişim yaşanıyor. Artık erkekler de bakımlarına ve estetik duruşlarına son derece önem veriyorlar</p>
                <p>Ülkemizde erkeklerin estetik cerrahiye artan taleplerini ve en fazla hangi prosedürü talep ettiklerini Estetik Plastik ve Rekonstrüktif Cerrahi Uzmanı Op. Dr. Bülent Cihantimur anlattı:</p><a class="right-btn" href="#"><i data-feather="arrow-right-circle"></i></a>
              </div>
            </div>
      <div class="row">
        <div class="col-md-12 text-center"><a class="see-more" href="#">SEE MORE</a></div>
      </div>
    </div>


       <?php } ?>

<?php endwhile; else: ?>
    <p><?php _e('No found'); ?></p>
<?php endif; ?>