
<div class="request-container">
      <div class="text-cont">
        <!-- <div class="lines"><span class="active">STEP 1</span><span>STEP 2</span><span>STEP 3</span><span>STEP 4</span><span>FINISH</span></div> -->
        <div class="bar"><span id="bar-rate" style="width: 20%;"></span></div>
        <h3>Request your free quote</h3>
        <input id="step1" class="form-control" type="text" placeholder="Name - Surname"><br>
        <input id="step2"  class="form-control" name="name" type="text" placeholder="Phone">
          <input id="step3" style="display:none;" class="form-control" name="operation" type="text" placeholder="What time are planning to do your operation">
          <input id="step4" style="display:none;" class="form-control" name="email" type="text" placeholder="Email">
          <input id="step5" style="display:none;" class="form-control" name="phone" type="text" placeholder="Phone">
          <input id="step6" style="display:none;" class="form-control" name="message"    type="text" placeholder="Message">
        <a class="btn btn-radius btn-success">Next Step<i data-feather="arrow-right-circle"></i></a>
        
      </div>
    </div>

      <script>
          window.onload = function()
      {     

        $('.btn').click(function() {
          if( $('#step1').val() == "" || $('#step2').val() == "" ) {
              alert('Please fill out this field.');
              $('#step1, #step2').css('border','1px solid red');

            } else {
              $('#step1, #step2').hide();
              $('#step3').show();
              $('#bar-rate').css('width','40%');
              return false;
            }
           
        });
        

          
        
            

      }
    </script>

     <section class="stat--area" id="block_four_add" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/block4/add/stats.jpg);">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12 col-md-12 col-lg-12 one">
            <h2>Aesthetic Treatments Stats</h2>
            <ul class="list-unstyled list-inline">
              <li class="col-md-2 col-xs-6 text-center"><img src="<?php bloginfo('template_url'); ?>/dist/img/block4/add/icon1.png" alt=""><br>156<br>
                <h5 class="visible-xs visible-sm">Total Operation</h5>
              </li>
              <li class="col-md-2 col-xs-6 text-center"><img src="<?php bloginfo('template_url'); ?>/dist/img/block4/add/icon2.png" alt=""><br>527
                <h5 class="visible-xs visible-sm">Our Patient Count</h5>
              </li>
              <li class="col-md-2 col-xs-6 text-center"><img src="<?php bloginfo('template_url'); ?>/dist/img/block4/add/icon3.png" alt=""><br>2567
                <h5 class="visible-xs visible-sm">Male Patient Count</h5>
              </li>
              <li class="col-md-2 col-xs-6 text-center"><img src="<?php bloginfo('template_url'); ?>/dist/img/block4/add/icon4.png" alt=""><br>3445
                <h5 class="visible-xs visible-sm">Patient Count</h5>
              </li>
              <li class="col-md-2 col-xs-6 text-center"><img src="<?php bloginfo('template_url'); ?>/dist/img/block4/add/icon5.png" alt=""><br>3445
                <h5 class="visible-xs visible-sm">Surgical Operation</h5>
              </li>
              <li class="col-md-2 col-xs-6 text-center"><img src="<?php bloginfo('template_url'); ?>/dist/img/block4/add/icon6.png" alt=""><br>3445
                <h5 class="visible-xs visible-sm">Hair Planting number</h5>
              </li>
            </ul>
            <ul class="list-unstyled list-inline add">
              <li class="col-md-2 col-xs-6 text-center hidden-xs hidden-sm"><i class="hidden-xs hidden-sm" data-feather="circle"></i><br><span class="text">Total Operation</span></li>
              <li class="col-md-2 col-xs-6 text-center hidden-xs hidden-sm"><i class="hidden-xs hidden-sm" data-feather="circle"></i><br><span class="text">Our Patient Count</span></li>
              <li class="col-md-2 col-xs-6 text-center hidden-xs hidden-sm"><i class="hidden-xs hidden-sm" data-feather="circle"></i><br><span class="text">Male Patient Count</span></li>
              <li class="col-md-2 col-xs-6 text-center hidden-xs hidden-sm"><i class="hidden-xs hidden-sm" data-feather="circle"></i><br><span class="text">Patient Count</span></li>
              <li class="col-md-2 col-xs-6 text-center hidden-xs hidden-sm"><i class="hidden-xs hidden-sm" data-feather="circle"></i><br><span class="text">Surgical Operation</span></li>
              <li class="col-md-2 col-xs-6 text-center hidden-xs hidden-sm"><i class="hidden-xs hidden-sm" data-feather="circle"></i><br><span class="text">Hair Planting number</span></li>
            </ul>
            <div class="divider-time col-md-10 hidden-xs hidden-md"></div>
          </div>
        </div>
      </div>
    </section>

  
