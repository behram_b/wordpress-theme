
<?php
          // Get the ID of a given category
          $face_id = get_cat_ID( 'face' );
          $face_link = get_category_link( $face_id );

          $body_id = get_cat_ID( 'body' );
          $body_link = get_category_link( $body_id );

          $breast_id = get_cat_ID( 'breast' );
          $breast_link = get_category_link( $breast_id );

          $genital_id = get_cat_ID( 'genital' );
          $genital_link = get_category_link( $genital_id );

          $nose_id = get_cat_ID( 'nose' );
          $nose_link = get_category_link( $nose_id );

          $liposuction_id = get_cat_ID( 'liposuction' );
          $liposuction_link = get_category_link( $liposuction_id );

          $butt_id = get_cat_ID( 'butt' );
          $butt_link = get_category_link( $butt_id );

          ?>

<div class="menu--area">
    <div class="mobile-nav"><a class="appointment" href="#">Free Appointment<i class="far fa-arrow-alt-circle-right"></i></a><a class="menu-toggle" id="menuToggle" href="#"><i class="fa fa-bars"></i></a></div>
    <ul id="menu">
        <li class="hasDropdown"><a class="active" href="#">Aesthetic Treatments</a>
            <ul class="dropdownFirst">
                <li><a href="<?php echo $nose_link ?>"><img src="<?php bloginfo('template_url'); ?>/dist/img/home-header/menu_face.png"><span>Face</span></a>
                    <ul class="dropdownSecond">
                        <li><a href="/spider-web-aesthetic">Spider Web Aesthetic</a></li>
                        <li><a href="/rhinoplasty">Rhinoplasty</a>
<!--                            <ul class="dropdownSecond">-->
<!--                                <li><a href="#">Rejuvenation</a></li>-->
<!--                                <li><a href="#">Incisionless Otoplasty</a></li>-->
<!--                                <li><a href="#">Otoplasty (Open Approach)</a></li>-->
<!--                                <li><a href="#">Blepharoplasty</a></li>-->
<!--                                <li><a href="#">Eyebrow Lifting</a></li>-->
<!--                                <li><a href="#">Lip Augmentation</a></li>-->
<!--                            </ul>-->
                        </li>
                        <li><a href="/simple-rhinoplasty">Simple Rhinoplasty</a></li>
                        <li><a href="/face-Lifting">Face Lifting</a></li>
                        <li><a href="/neck-lifting">Neck Lifting</a></li>
                        <li><a href="/total-face">Total Face </a></li>
                    </ul>
                </li>
                <li><a href="<?php echo $hair_link?>"> <img src="<?php bloginfo('template_url'); ?>/dist/img/home-header/menu_hair.png"><span>Hair</span></a>
                    <ul class="dropdownSecond">
                        <li><a href="/organic-hair-transplant">Organic Hair Transplant</a></li>
<!--                        <li><a href="#">Rhinoplasty</a>-->
<!--                            <ul class="dropdownSecond">-->
<!--                                <li><a href="#">Rejuvenation</a></li>-->
<!--                                <li><a href="#">Incisionless Otoplasty</a></li>-->
<!--                                <li><a href="#">Otoplasty (Open Approach)</a></li>-->
<!--                                <li><a href="#">Blepharoplasty</a></li>-->
<!--                                <li><a href="#">Eyebrow Lifting</a></li>-->
<!--                                <li><a href="#">Lip Augmentation</a></li>-->
<!--                            </ul>-->
<!--                        </li>-->
                        <li><a href="/fue-hair-transplantation">FUE Hair Transplantation</a></li>
                        <li><a href="/platelet-rich-plasma-(PRP)">Platelet Rich Plasma (PRP)</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo $breast_link?>"> <img src="<?php bloginfo('template_url'); ?>/dist/img/home-header/menu_breast.png"><span>Breast</span></a>
                    <ul class="dropdownSecond">
                        <li><a href="/breast-augmentation">Breast Augmentation</a></li>
<!--                        <li><a href="#">Rhinoplasty</a>-->
<!--                            <ul class="dropdownSecond">-->
<!--                                <li><a href="#">Rejuvenation</a></li>-->
<!--                                <li><a href="#">Incisionless Otoplasty</a></li>-->
<!--                                <li><a href="#">Otoplasty (Open Approach)</a></li>-->
<!--                                <li><a href="#">Blepharoplasty</a></li>-->
<!--                                <li><a href="#">Eyebrow Lifting</a></li>-->
<!--                                <li><a href="#">Lip Augmentation</a></li>-->
<!--                            </ul>-->
<!--                        </li>-->
                        <li><a href="/breast-reduction">Breast Reduction</a></li>
                        <li><a href="/Breast Lifting">Breast Lifting</a></li>
                    </ul>
                </li>
                <li><a href="<?php echo $body_link?>"> <img src="<?php bloginfo('template_url'); ?>/dist/img/home-header/menu_body.png"><span>Body</span></a>
                    <ul class="dropdownSecond">
                        <li><a href="/gynecomastia">Gynecomastia</a></li>
                        <li><a href="/tummy-tuck">Tummy Tuck</a>
<!--                            <ul class="dropdownSecond">-->  
<!--                                <li><a href="#">Rejuvenation</a></li>-->
<!--                                <li><a href="#">Incisionless Otoplasty</a></li>-->
<!--                                <li><a href="#">Otoplasty (Open Approach)</a></li>-->
<!--                                <li><a href="#">Blepharoplasty</a></li>-->
<!--                                <li><a href="#">Eyebrow Lifting</a></li>-->
<!--                                <li><a href="#">Lip Augmentation</a></li>-->
<!--                            </ul>-->
                        </li>
                        <li><a href="/genital-beautification">Genital Beautification</a></li>
                        <li><a href="/cihantimur_fat_transfer">Cihantimur Fat Transfer</a></li>
                        <li><a href="/nutational-ınfrasonic-liposculpture">Nutational Infrasonic Liposculpture</a></li>
                        <li><a href="laser-lipolysis">Laser Lipolysis</a></li>
                        <li><a href="weight-loss-surgery">Weight Loss Surgery</a></li>
                    </ul>
                </li>
            </ul>
        </li>
        <?php

        wp_nav_menu( array(
            'theme_location'	=> 'header-menu',
            'depth'				=> 1, // 1 = with dropdowns, 0 = no dropdowns.
            'container'			=> 'menu--area',
            'container_class'	=> 'menu--area',
            'container_id'		=> 'bs-example-navbar-collapse-1',
            'items_wrap'      => '<li id=\"%1$s\" class=\"%2$s\">%3$s</li>',    
            'menu_class'		=> 'menu',
            'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
            'walker'			=> new WP_Bootstrap_Navwalker()
        ) );

        // wp_nav_menu( array(
        //     'theme_location' => 'header-menu',
        //     'container'       => 'menu--area',
        //     'menu_class'         => 'menu',
        //     'walker' => new Microdot_Walker_Nav_Menu(),
        //     'items_wrap'      => '<li id=\"%1$s\" class=\"%2$s\">%3$s</li>',    
        // ) );
        ?>
    </ul>
</div>

<!-- <nav class="navbar navbar-default" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    <a class="navbar-brand" href="#">ISW</a>
    </div>
 
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <?php
        // wp_nav_menu( array(
        //     'theme_location' => 'header-menu',
        //     'depth' => 2,
        //     'container' => false,
        //     'menu_class' => 'nav navbar-nav',
        //     'fallback_cb' => 'wp_page_menu',
        //     //Process nav menu using our custom nav walker
        //     'walker' => new wp_bootstrap_navwalker())
        // );
        ?>
    </div><
</nav> -->