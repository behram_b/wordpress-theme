<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

    <?php
    if ( !empty( get_the_content() ) ) {
        the_content();
    } else {

        ?>
        <section class="page--section">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="title-area">
                    <h2>
                        <b>Treatment</b> Information</h2>
                </div>
            </div>
            <div class="col-md-6 col-xs-12 text-right">
                <p class="description">
                    <b class="green">Estetik International</b>, founded as a superior aesthetic and beauty centre by renowned surgeon
                    <b class="dark">Dr. Bulent Cihantimur</b> in 1999, is a medical foundation performing the aesthetic procedures and changes desired by its patients through clinical branches equipped with the latest and best technological apparatuses. Through significant medical and scientific focus, Dr. Cihantimur and his team at Estetik International have adopted the best practices and skills for achieving perfect results every time.</p>
            </div>
            <div class="col-md-6 col-xs-12 text-left">
                <p class="small-description bordered">Estetik International became one of the most important and respectable health groups in
                    <b class="dark">Turkey</b> during recent times. Most notably, they strive to increase the quality of members and patients’ lives
                    <b class="dark">experts </b> their total of Six service clinics in Istanbul as well as three locations in the Byomed Medical Centre in Ankara, Bursa, and Izmir. Estetik International’s professional staff of 150 people consists of those who are experts in their fields, practising their jobs passionately for at least 15 years. The Estetik International foundation, which boasts the achievement of holding multiple Certificates of Quality in the Field of Health, is a strict believer in and activist for improving innovative technologies. The group continually makes investments particularly toward RESEARCH and DEVELOPMENT. The laboratory department as well has received many investment renewals under the leadership of Dr. Bülent Cihantimur. At Estetik International, expert physicians in a range of medical specialities as well as an experienced staff together create an atmosphere that makes greater beauty in your life possible. The team is so devoted to your happiness and well-being that they are even open on Saturdays.</p>
            </div>
        </div>
    </div>
</section>
<section class="page--section cover-bg" style="background: url(../wp-content/uploads/2018/02/about-2.jpg);">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 text-center">
                <div class="title-area">
                    <hr class="default">
                    <h2 class="is-white">Why Estetik International?</h2>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="reason-item">
                    <i class="fa fa-check-square"></i>
                    <p>The physicians and staff are experts in the field of aesthetic surgery field, especially under the guidance and direction of renowned surgeon, Dr. Bülent Cihantimur,</p>
                </div>
                <div class="reason-item">
                    <i class="fa fa-check-square"></i>
                    <p>The professional health personnel and employees make up a comprehensive and knowledgeable team of 150 people skills in the aesthetic surgery field,</p>
                </div>
                <div class="reason-item">
                    <i class="fa fa-check-square"></i>
                    <p>New, effective and useful procedures supported by the research and development work of the Estetik International team,</p>
                </div>
                <div class="reason-item">
                    <i class="fa fa-check-square"></i>
                    <p>Innovative, modern and inquisitive sub-structure,</p>
                </div>
                <div class="reason-item">
                    <i class="fa fa-check-square"></i>
                    <p>Latest technological devices used in surgery,</p>
                </div>
            </div>
            <div class="col-md-6 col-xs-12">
                <div class="reason-item">
                    <i class="fa fa-check-square"></i>
                    <p>Sensitive policies caring especially for the patient’s pleasure and confidence,</p>
                </div>
                <div class="reason-item">
                    <i class="fa fa-check-square"></i>
                    <p>Safe, sterilized and modern campuses,</p>
                </div>
                <div class="reason-item">
                    <i class="fa fa-check-square"></i>
                    <p>Customer services where you can get clear and all-encompassing feedback. Spider Web Aesthetic, Organic Hair Transplantaion, Cihantimur’s Fat Transfer System, Simple Nose Aesthetic, Trigger Finger’s treatment without Operation… And many more. All these applications and techniques are the result of a clinic-wide focus on promoting the comfort, quality, and humanity of every patient’s needs and wants. We always aim to be the best in the industry, pulling out all the stops to maintain this stature. Estetik International, which was founded with the utmost attention– the name itself even took one year to be decided on– in 1999, today experiences the pride of being a branded health group with superb results all while also serving internationally.</p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="page--section">
    <div class="container-fluid no-padding">
        <div class="row">
            <div class="col-md-12">
                <div class="inner-title-area">
                    <h3>Our
                        <span>Team</span>
                        <small>There are many variations of
                            <br> passages of Lorem Ipsum available</small>
                    </h3>
                </div>
            </div>
            <div class="col-md-12">
                <div class="col-lg-3 col-md-3 col-xs-6">
                    <a class="team-person" style="background: url(../wp-content/uploads/2018/02/user.png);" href="#">
              <span class="name">Salih
                <br>Budak</span>
                        <span class="job">Manager</span>
                        <span class="next">
                <i class="fa fa-paper-plane"></i>
              </span>
                    </a>
                    <a class="team-person" style="background: url(../wp-content/uploads/2018/02/user.png);" href="#">
              <span class="name">Salih
                <br>Budak</span>
                        <span class="job">Manager</span>
                        <span class="next">
                <i class="fa fa-paper-plane"></i>
              </span>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-6">
                    <a class="team-person" style="background: url(../wp-content/uploads/2018/02/user.png);" href="#">
              <span class="name">Salih
                <br>Budak</span>
                        <span class="job">Manager</span>
                        <span class="next">
                <i class="fa fa-paper-plane"></i>
              </span>
                    </a>
                    <a class="team-person" style="background: url(../wp-content/uploads/2018/02/user.png);" href="#">
              <span class="name">Salih
                <br>Budak</span>
                        <span class="job">Manager</span>
                        <span class="next">
                <i class="fa fa-paper-plane"></i>
              </span>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-6">
                    <a class="team-person" style="background: url(../wp-content/uploads/2018/02/user.png);" href="#">
              <span class="name">Salih
                <br>Budak</span>
                        <span class="job">Manager</span>
                        <span class="next">
                <i class="fa fa-paper-plane"></i>
              </span>
                    </a>
                    <a class="team-person" style="background: url(../wp-content/uploads/2018/02/user.png);" href="#">
              <span class="name">Salih
                <br>Budak</span>
                        <span class="job">Manager</span>
                        <span class="next">
                <i class="fa fa-paper-plane"></i>
              </span>
                    </a>
                </div>
                <div class="col-lg-3 col-md-3 col-xs-6">
                    <a class="team-person" style="background: url(../wp-content/uploads/2018/02/user.png);" href="#">
              <span class="name">Salih
                <br>Budak</span>
                        <span class="job">Manager</span>
                        <span class="next">
                <i class="fa fa-paper-plane"></i>
              </span>
                    </a>
                    <a class="team-person" style="background: url(../wp-content/uploads/2018/02/user.png);" href="#">
              <span class="name">Salih
                <br>Budak</span>
                        <span class="job">Manager</span>
                        <span class="next">
                <i class="fa fa-paper-plane"></i>
              </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="page--section cover-bg" style="background: url(wp-content/uploads/2018/02/about-3.jpg);">
    <div class="container-fluid mv--area">
        <div class="row">
            <div class="col-md-6 col-xs-12">
                <h3>Our Vision</h3>
                <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
            </div>
            <div class="col-md-6 col-xs-12">
                <h3>Our Mission</h3>
                <p>All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum which looks reasonable. The generated Lorem Ipsum is therefore always free from repetition, injected humour, or non-characteristic words etc.</p>
            </div>
        </div>
    </div>
</section>

   <?php } ?>




<?php endwhile; else: ?>
    <p><?php _e('no found'); ?></p>
<?php endif; ?>