<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<header class="main-header" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/bg/about.jpg);">
      <div class="container-fluid">
        <nav class="navi-area">
          <div class="logo-area"><a href="#"><img src="<?php bloginfo('template_url'); ?>/dist/img/logo.svg" alt="Logo"></a></div>
          <div class="right-area"><a href="#"><img src="img/icons/whatsapp.svg" style="margin-right: 10px;" alt=""><span>+90</span> 444 77 07</a><a class="btn-appointment" href="#">Free Appointment<i data-feather="arrow-right-circle"></i></a></div>
        </nav>
      </div>
      <?php get_template_part('inc/navbar_menus'); ?>
      <div class="hero-title text-center">
      <h1><?php the_title(); ?></h1><!--            <b class="color-white">Aesthetic</b>-->
        <p><?php the_excerpt(); ?></p>
        <div class="play-area"><a class="play" href="#"><i data-feather="play"></i></a><small>View Estetik International Trailer</small></div>
        <div class="option-buttons"><a class="active" href="#"><b>Treatment </b> Information</a><a href="#"><b>Why </b> Estetik International?</a></div>
      </div>
    </header>

<?php endwhile; else: ?>
    <p><?php _e('No found'); ?></p>
<?php endif; ?>


