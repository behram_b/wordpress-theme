
<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

<?php
if ( !empty( get_the_content() ) ) {
    the_content();
} else {

    ?>

     <div class="container-fluid">
      <div class="test-item-cont">
        <div class="info-sec" style="background: url(img/bg/clinic-2.jpg);">
          <h2>Istanbul<small>(Byomed)</small></h2>
        </div>
        <div class="desc">
          <p>Our Byomed branch in Kozyatağı district of Istanbul, serves at the superior level with its full equipped medical substructure and professionals expert at their subjects. Byomed assists in subjects of the aesthetical surgery and medical applications to our cross-border patients came through the health tourism and throughout Turkey. We effort for the happiness of our guests with over 50 employees of us in our Byomed Branch in Istanbul, serving with the conscious that it is a living right and the most basic humanistic right for our patients to feel good physically, spiritually and socially as the most basic right.</p><br>
          <p>The services given in Byomed clinic, where the research and development studies are made and the new technologies are developed particularly upon rejuvenation without any operation: All aesthetical surgical operations and medical applications, hair transplantation, Organic Hair Transplantation, Spider Web Aesthetic, hair shedding and scalp’s treatments, laser treatments and laser epilation, ozone therapy, face rejuvenation, our last techniques, diet and healthy feeding, all techniques without any operations slimming and cellulite treatments.</p>
        </div>
      </div>
    </div>
    <div class="clinic-map" id="map">
      <iframe src="https://www.google.com/maps/embed?pb=!1m16!1m12!1m3!1d2965.0824050173574!2d-93.63905729999999!3d41.998507000000004!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!2m1!1sWebFilings%2C+University+Boulevard%2C+Ames%2C+IA!5e0!3m2!1sen!2sus!4v1390839289319" frameborder="0" style="border:0;"></iframe>
    </div>
    <div class="container-fluid">
      <div class="title--area">
        <h5>VIDEO<span> GALLERY</span></h5>
        <div class="slider-area hoverable"><a class="video-item is-long hoverable" style="background: url(img/video-item-2.jpg);" href="#">
            <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır. Ekim öncesinde saç derisine enjekte edilen sıvıda bol miktarda kök hücre bulunur.</span></span></a><a class="video-item is-long hoverable" style="background: url(img/video-item-2.jpg);" href="#">
            <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır. Ekim öncesinde saç derisine enjekte edilen sıvıda bol miktarda kök hücre bulunur.</span></span></a><a class="video-item is-long hoverable" style="background: url(img/video-item-2.jpg);" href="#">
            <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır. Ekim öncesinde saç derisine enjekte edilen sıvıda bol miktarda kök hücre bulunur.</span></span></a><a class="video-item is-long hoverable" style="background: url(img/video-item-2.jpg);" href="#">
            <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır. Ekim öncesinde saç derisine enjekte edilen sıvıda bol miktarda kök hücre bulunur.</span></span></a><a class="video-item is-long hoverable" style="background: url(img/video-item-2.jpg);" href="#">
            <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır. Ekim öncesinde saç derisine enjekte edilen sıvıda bol miktarda kök hücre bulunur.</span></span></a><a class="video-item is-long hoverable" style="background: url(img/video-item-2.jpg);" href="#">
            <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır. Ekim öncesinde saç derisine enjekte edilen sıvıda bol miktarda kök hücre bulunur.</span></span></a><a class="video-item is-long hoverable" style="background: url(img/video-item-2.jpg);" href="#">
            <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır. Ekim öncesinde saç derisine enjekte edilen sıvıda bol miktarda kök hücre bulunur.</span></span></a><a class="video-item is-long hoverable" style="background: url(img/video-item-2.jpg);" href="#">
            <div class="play_item"><i data-feather="play"></i></div><span><span class="title">Organik Saç Ekimi</span><span class="description">Organik Saç Ekimi tekniğinde kişinin kendi vücudundaki yağdan temin edilen kök hücreler kullanılmaktadır. Ekim öncesinde saç derisine enjekte edilen sıvıda bol miktarda kök hücre bulunur.</span></span></a></div>
      </div>
    </div>
    <section class="page--section consultation" style="background: url(img/bg/consultation.jpg);"><a href="#"><span class="title">Free Online Consultation</span><i data-feather="arrow-right-circle"></i></a></section>


    <?php } ?>


<?php endwhile; else: ?>
    <p><?php _e('Bulunamadı'); ?></p>
<?php endif; ?>