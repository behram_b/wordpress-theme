<?php /* Template Name: News Page */ ?>

<?php get_template_part('head'); ?>
<?php get_template_part('inc/news/header'); ?>
<?php get_template_part('inc/news/content'); ?>
<?php get_template_part('footer'); ?>