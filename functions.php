<?php

/* Theme Dashboard */
if ( STYLESHEETPATH == TEMPLATEPATH ) {
    define('OF_FILEPATH', TEMPLATEPATH);
    define('OF_DIRECTORY', get_bloginfo('template_directory'));
} else {
    define('OF_FILEPATH', STYLESHEETPATH);
    define('OF_DIRECTORY', get_bloginfo('stylesheet_directory'));
}
require_once (OF_FILEPATH . '/admin/admin-functions.php');
require_once (OF_FILEPATH . '/admin/admin-interface.php');
require_once (OF_FILEPATH . '/admin/theme-options.php');
require_once (OF_FILEPATH . '/admin/theme-functions.php');
/* #TEMA PANELI */

//Text-Editor - <p> and <br> tags to your content remove

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );

remove_filter('the_content', 'wptexturize');
remove_filter('the_excerpt', 'wptexturize');
remove_filter('comment_text', 'wptexturize');
remove_filter('the_title', 'wptexturize');




//SVG ALLOWED

function cc_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter('upload_mimes', 'cc_mime_types');


//// Register Custom Navigation Walker
//require_once get_template_directory() . '/wp-bootstrap-navwalker.php';
//register_nav_menus( array(
//    'primary' => __( 'Primary Menu', 'estetikinternational-theme' ),
//) );
//
//
//function cc_mime_types($mimes) {
//    $mimes['svg'] = 'image/svg+xml';
//    return $mimes;
//}
//add_filter('upload_mimes', 'cc_mime_types');


// Custom Menu
// function wpb_custom_new_menu() {
//     register_nav_menu('my-custom-menu',__( 'My Custom Menu' ));
//     register_nav_menu('footer-menu',__( 'Footer Menu' ));

// }
// add_action( 'init', 'wpb_custom_new_menu' );

// function register_my_menus() {
//     register_nav_menus(
//       array(
//         'header-menu' => __( 'Header Menu' ),
//         'extra-menu' => __( 'Footer Menu' ),
//       )
//     );
//   }
  
//   add_action( 'init', 'register_my_menus' );

// Register Custom Navigation Walker

// Register Custom Navigation Walker
require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';

register_nav_menus( array(
  'header-menu' => __( 'Header Menu' ),
  'footer-menu' => __( 'Footer Menu' ),
) );

  
  
// /* Öne Çıkarılmış Görsel Özelliği Ekliyoruz */
 
// if (function_exists('add_theme_support')) { 
 
//     add_theme_support( 'post-thumbnails' );
//     set_post_thumbnail_size( 640, 480, true ); // Standart Değerler
//     add_image_size('img-small', 250, 175, true); // Küçük Resmimiz
//     add_image_size('img-small', 450, 290, true); // Büyük Resmimiz
  
//  /* dilediğimiz kadar boyut ve alan ekleyebiliriz */
//  /* örnek: add_image_size('ne-isterseniz', 999, 999, true); */
  
//  }

//  /* eğer direk resmi göstermek istiyorsak bu kodu kullanacağız */
 
// the_post_thumbnail('img-small'); 
 
// /* eğer bir seçici ( class ) eklemek istiyorsak bu kodu kullanacağız */
 
// the_post_thumbnail('img-small', array('class' => 'thumbnail'));  
 
// /* " seçici " kısmına class adını yazarak kullanabilirsiniz */
// /* " resim-kucuk " kısmına yazdığımız değeri fonksiyonlar dosyamızda oluşturduk. */


add_theme_support( 'post-thumbnails' );
if (class_exists('MultiPostThumbnails')) {
    new MultiPostThumbnails(
        array(
            'label' => 'Secondary Image',
            'id' => 'secondary-image',
            'post_type' => 'post'
        )
    );
}


add_action( 'genesis_setup', 'leaven_setup', 15 );
function leaven_setup() {
  include_once( get_stylesheet_directory() . '/includes/admin.php' );
}