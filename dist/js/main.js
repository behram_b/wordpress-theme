function drags(e, a, t) {
  e.on("mousedown touchstart", function(i) {
    e.addClass("draggable"), a.addClass("resizable");
    var s = i.pageX ? i.pageX : i.originalEvent.touches[0].pageX,
      o = e.outerWidth(),
      n = e.offset().left + o - s,
      l = t.offset().left,
      u = t.outerWidth();
    minLeft = l + 10, maxLeft = l + u - o - 10, e.parents().on("mousemove touchmove", function(e) {
      var t = e.pageX ? e.pageX : e.originalEvent.touches[0].pageX;
      leftValue = t + n - o, leftValue < minLeft ? leftValue = minLeft : leftValue > maxLeft && (leftValue = maxLeft), widthValue = 100 * (leftValue + o / 2 - l) / u + "%", $(".draggable").css("left", widthValue).on("mouseup touchend touchcancel", function() { $(this).removeClass("draggable"), a.removeClass("resizable") }), $(".resizable").css("width", widthValue)
    }).on("mouseup touchend touchcancel", function() { e.removeClass("draggable"), a.removeClass("resizable") }), i.preventDefault()
  }).on("mouseup touchend touchcancel", function(t) { e.removeClass("draggable"), a.removeClass("resizable") })
}
$(function() {
  var e = $("#menuToggle"),
    a = $("#menu");
  e.click(function(e) { a.slideToggle(), e.preventDefault() });
  $(".mobile-nav");
  var t = $(".menu--area");
  $(window).scroll(function() { $(this).scrollTop() > 200 ? t.addClass("is-fixed") : t.removeClass("is-fixed") }), $(".ba-slider").each(function() {
    var e = $(this),
      a = e.width() + "px";
    e.find(".resize img").css("width", a), drags(e.find(".handle"), e.find(".resize"), e)
  }), $(".two-items-slider").owlCarousel({ navigation: !1, pagination: !1, items: 2, 768: { items: 2 }, 1200: { items: 2 } }), $(".slider-area").owlCarousel({ navigation: !1, pagination: !1, items: 3, 768: { items: 2 }, 1200: { items: 2 } })
}), $(window).resize(function() {
  $(".ba-slider").each(function() {
    var e = $(this),
      a = e.width() + "px";
    e.find(".resize img").css("width", a)
  })
});

feather.replace()

$.fn.toggleText = function(t1, t2) {
  if (this.html() == t1) this.html(t2);
  else this.html(t1);
  return this;
};

$('#menuToggle').click(function() {
  $(this)
    .toggleClass('is-active')
    .toggleText(
      '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-x"><line x1="18" y1="6" x2="6" y2="18"></line><line x1="6" y1="6" x2="18" y2="18"></line></svg>',
      '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-menu"><line x1="3" y1="12" x2="21" y2="12"></line><line x1="3" y1="6" x2="21" y2="6"></line><line x1="3" y1="18" x2="21" y2="18"></line></svg>'
    )
})

$('.toggle-next-menu').click(function() {
  $(this)
    .toggleText(
      '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-minus"><line x1="5" y1="12" x2="19" y2="12"></line></svg>',
      '<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-plus"><line x1="12" y1="5" x2="12" y2="19"></line><line x1="5" y1="12" x2="19" y2="12"></line></svg>'
    )
    .next('ul').toggleClass('is-mobile-active')
})

$('.search--area button').click(function() {
  $(this).parent().find('input').toggleClass('is-active')
})

if ($(window).width() < 760) {
  $(".mobile-slider-area").owlCarousel({ navigation: !1, pagination: !1, items: 2, 768: { items: 2 }, 1200: { items: 2 } })
}

$('.dropdownFirst li > a').click(function() {
  $(this).toggleClass('test')
})

$("[data-fancybox]").fancybox();

$('.slider-area-single').owlCarousel({ navigation: !1, pagination: !1, singleItem: true, autoPlay: 3000 })
