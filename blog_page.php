<?php /* Template Name: Blog Page */ ?>

<?php get_template_part('head'); ?>
<?php get_template_part('inc/blog/header'); ?>
<?php get_template_part('inc/blog/content'); ?>
<?php get_template_part('footer'); ?>
