
    <div class="mg_top_57"></div>
    <section id="block">
      <div class="text-center">
        <h2 class="main--title">BEFORE &amp; <span>AFTER</span></h2>
        <h3 class="secondary--title">Drag slider bar left and right to view</h3>
      </div><br>
      <div class="container-fluid">
        <div class="row">

            <?php  
            $args=array(
            'cat' => 19,
            'orderby' => rand,
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 4,
            'caller_get_posts'=> 1,

            );
            $my_query = null;
            $my_query = new WP_Query($args);
            if( $my_query->have_posts() ) {
            while ($my_query->have_posts()) : $my_query->the_post(); ?>

         <?php $blog_id = get_option( 'page_for_posts' );
        $blog_image = MultiPostThumbnails::get_post_thumbnail_url( get_post_type(), 'secondary-image', $blog_id, 'full' );
               
        ?>     
          <div class="col-xs-12 col-md-3 hidden-xs"><a class="thumbnail">
             <a href="<?php echo get_page_link(29757); ?>"><div class="icon_bf"><img class="img-responsive" src="<?php bloginfo('template_url'); ?>/dist/img/block/nose_icon.png" alt="nose "></div></a> 
              <div class="ba-slider"><img src="<?php the_post_thumbnail_url() ?>">
                <div class="resize"><img src="<?php echo $blog_image ?>"></div><span class="handle"></span>
              </div></a></div>
              <?php endwhile;   } wp_reset_query(); ?>
          <div class="col-xs-12 visible-xs text-center"><a class="button-1 btn-block" href="#" style="background: #17c9ad;min-height: 50px!important; border-radius: 50px">SEE ALL</a><br><br></div>
        </div>
      </div>
    </section>
    

    <!-- real stories-->
    <div class="container-fluid">
      <section class="row" id="block">
        <div class="text-center col-md-12">
          <h2 class="main--title">REAL &amp; <span>STORIES</span></h2>
          <h3 class="secondary--title">Listen to the real stories of our clients</h3>
        </div>
        <?php
            $args=array(
            'cat' => 21,
            'orderby' => rand,
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 2,
            'caller_get_posts'=> 1
            );
            $my_query = null;
            $my_query = new WP_Query($args);
            if( $my_query->have_posts() ) {
            while ($my_query->have_posts()) : $my_query->the_post(); ?>

           <div class="col-md-6 col-xs-12">
          <div class="real-item">
            <div class="desc">
            
            <a href="<?php permalink_link() ?>"><p><?php the_excerpt() ?></p></a>
              <a class="button-pink" href="<?php the_permalink() ?> ">READ MORE <i data-feather="arrow-right-circle"></i></a><a class="read-more" href="<?php the_permalink() ?>"><i data-feather="play"></i></a>
            </div><img src="<?php the_post_thumbnail_url() ?>" alt="">
          </div>
        </div>
        
        <?php endwhile;   } wp_reset_query();  // Restore global post data stomped by the_post().  ?>
      </section>
    </div>

    <!-- home gallery-->
    <section class="gallery-area">
      <div class="contaniner-fluid">
        <div class="row flex">
        <?php
            $args=array(
            'cat' => 16,
            'orderby' => rand,
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 1,
            'caller_get_posts'=> 1
            );
            $my_query = null;
            $my_query = new WP_Query($args);
            if( $my_query->have_posts() ) {
            while ($my_query->have_posts()) : $my_query->the_post(); ?>

            <div class="col-md-6 col-xs-12 gallery-item" style="background: url(<?php the_post_thumbnail_url() ?>);">
              <div class="title"><?php the_title(); ?></div>
              <p><?php the_excerpt() ?></p><a class="button-1 mt-2" href="<?php the_permalink() ?>">Read More <i data-feather="arrow-right-circle" style="color:white;"></i></a><br>
            </div>

        <?php endwhile;   } wp_reset_query();  // Restore global post data stomped by the_post().  ?>
      
        
        <?php
            $args=array(
            'cat' => 17,
            'orderby' => rand,
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 1,
            'caller_get_posts'=> 1
            );
            $my_query = null;
            $my_query = new WP_Query($args);
            if( $my_query->have_posts() ) {
            while ($my_query->have_posts()) : $my_query->the_post(); ?>

            <div class="col-md-6 col-xs-12 hidden-xs"><a class="gallery-item" href="<?php the_permalink() ?>" style="background: url(<?php the_post_thumbnail_url() ?>);">
            <div class="title"><?php the_title(); ?></div>

              <?php endwhile; }  wp_reset_query();  // Restore global post data stomped by the_post(). ?>   
      

          <?php
            $args=array(
            'cat' => 17,
            'orderby' => rand,
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 1,
            'caller_get_posts'=> 1
            );
            $my_query = null;
            $my_query = new WP_Query($args);
            if( $my_query->have_posts() ) {
            while ($my_query->have_posts()) : $my_query->the_post(); ?>

          <p><?php the_excerpt() ?></p></a><a class="gallery-item" href="<?php the_permalink() ?>" style="background: url(<?php the_post_thumbnail_url() ?>);">
          <div class="title"><?php the_title(); ?></div>
          <p><?php the_excerpt() ?></p></a></div>
          <?php endwhile;  }  wp_reset_query();  // Restore global post data stomped by the_post().   ?>    </div>

    
          
      </div>
    </section>
    <section class="home-clinics">
      <div class="container-fluid slider-area-single">
    <?php
            $args=array(
            'cat' => 20,
            'orderby' => rand,
            'post_type' => 'post',
            'post_status' => 'publish',
            'posts_per_page' => 20,
            'caller_get_posts'=> 1
            );
            $my_query = null;
            $my_query = new WP_Query($args);
            if( $my_query->have_posts() ) {
            while ($my_query->have_posts()) : $my_query->the_post(); ?>
        <div class="row flex align-center">
          <div class="col-md-7 col-xs-12 hidden-xs hidden-md"><img class="banner" src="<?php the_post_thumbnail_url() ?>" alt="clinic"></div>
          <div class="col-lg-5 col-md-12 col-xs-12">
            <div class="pink-bg">
          <a href="<?php the_permalink() ?>"><h5><?php the_title(); ?></h5></a>
              <a href="<?php the_permalink() ?>"><p><?php the_excerpt() ?></p></a>
            </div>
          </div>
        </div>
        <?php endwhile;  }  wp_reset_query();  // Restore global post data stomped by the_post().   ?>    </div>

     
    </section>
    <div class="space_25 hidden-md hidden-xs visable-lg"></div>
    <div class="space_25 hidden-xs"></div>          
   
    <section class="stat--area" id="block_four_add" style="background: url(<?php bloginfo('template_url'); ?>/dist/img/block4/add/stats.jpg);">
      <div class="container-fluid">
        <div class="row">
          <div class="col-xs-12 col-md-12 col-lg-12 one">
            <h2>Aesthetic Treatments Stats</h2>
            <ul class="list-unstyled list-inline">
              <li class="col-md-2 col-xs-6 text-center"><img src="<?php bloginfo('template_url'); ?>/dist/img/block4/add/icon1.png" alt=""><br><?php echo get_option('est_total') ?><br>
                <h5 class="visible-xs visible-sm">Total Operation</h5>
              </li>
              <li class="col-md-2 col-xs-6 text-center"><img src="<?php bloginfo('template_url'); ?>/dist/img/block4/add/icon2.png" alt=""><br><?php echo get_option('est_our_count') ?>
                <h5 class="visible-xs visible-sm">Our Patient Count</h5>
              </li>
              <li class="col-md-2 col-xs-6 text-center"><img src="<?php bloginfo('template_url'); ?>/dist/img/block4/add/icon3.png" alt=""><br><?php echo get_option('est_male_count') ?>
                <h5 class="visible-xs visible-sm">Male Patient Count</h5>
              </li>
              <li class="col-md-2 col-xs-6 text-center"><img src="<?php bloginfo('template_url'); ?>/dist/img/block4/add/icon4.png" alt=""><br><?php echo get_option('est_patient_count') ?>
                <h5 class="visible-xs visible-sm">Patient Count</h5>
              </li>
              <li class="col-md-2 col-xs-6 text-center"><img src="<?php bloginfo('template_url'); ?>/dist/img/block4/add/icon5.png" alt=""><br><?php echo get_option('est_surgical') ?>
                <h5 class="visible-xs visible-sm">Surgical Operation</h5>
              </li>
              <li class="col-md-2 col-xs-6 text-center"><img src="<?php bloginfo('template_url'); ?>/dist/img/block4/add/icon6.png" alt=""><br><?php echo get_option('est_hair') ?>
                <h5 class="visible-xs visible-sm">Hair Planting number</h5>
              </li>
            </ul>
            <ul class="list-unstyled list-inline add">
              <li class="col-md-2 col-xs-6 text-center hidden-xs hidden-sm"><i class="hidden-xs hidden-sm" data-feather="circle"></i><br><span class="text">Total Operation</span></li>
              <li class="col-md-2 col-xs-6 text-center hidden-xs hidden-sm"><i class="hidden-xs hidden-sm" data-feather="circle"></i><br><span class="text">Our Patient Count</span></li>
              <li class="col-md-2 col-xs-6 text-center hidden-xs hidden-sm"><i class="hidden-xs hidden-sm" data-feather="circle"></i><br><span class="text">Male Patient Count</span></li>
              <li class="col-md-2 col-xs-6 text-center hidden-xs hidden-sm"><i class="hidden-xs hidden-sm" data-feather="circle"></i><br><span class="text">Patient Count</span></li>
              <li class="col-md-2 col-xs-6 text-center hidden-xs hidden-sm"><i class="hidden-xs hidden-sm" data-feather="circle"></i><br><span class="text">Surgical Operation</span></li>
              <li class="col-md-2 col-xs-6 text-center hidden-xs hidden-sm"><i class="hidden-xs hidden-sm" data-feather="circle"></i><br><span class="text">Hair Planting number</span></li>
            </ul>
            <div class="divider-time col-md-10 hidden-xs hidden-md"></div>
          </div>
        </div>
      </div>
    </section>
    <div class="clearfix visible-xs"></div>
    