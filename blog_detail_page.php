<?php /* Template Name: Blog Detail Page */ ?>

<?php get_template_part('head'); ?>
<?php get_template_part('inc/blog/detail/header'); ?>
<?php get_template_part('inc/blog/detail/content'); ?>
<?php get_template_part('footer'); ?>
