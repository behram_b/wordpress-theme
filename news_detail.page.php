<?php /* Template Name: News Detail Page */ ?>
<?php get_template_part('head'); ?>
<?php get_template_part('inc/news/detail/header'); ?>
<?php get_template_part('inc/news/detail/content'); ?>
<?php get_template_part('footer'); ?>
