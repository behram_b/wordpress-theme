<?php /* Template Name: Testimonials */ ?>

<?php get_template_part('head'); ?>
<?php get_template_part('inc/testimonials/header'); ?>
<?php get_template_part('inc/testimonials/content'); ?>
<?php get_template_part('footer'); ?>
