<?php

add_action('init', 'of_options');

if (!function_exists('of_options')) {
    function of_options()
    {

// VARIABLES
        $themename = get_theme_data(STYLESHEETPATH . '/style.css');
        $themename = $themename['Name'];
        $shortname = "est";

// Populate OptionsFramework option in array for use in theme
        global $of_options;
        $of_options = get_option('of_options');

        $GLOBALS['template_path'] = OF_DIRECTORY;

//Access the WordPress Categories via an Array
        $of_categories = array();
        $of_categories_obj = get_categories('hide_empty=0');
        foreach ($of_categories_obj as $of_cat) {
            $of_categories[$of_cat->cat_ID] = $of_cat->cat_name;
        }
        $categories_tmp = array_unshift($of_categories, "Kategori seçiniz:");

//Access the WordPress Pages via an Array
        $of_pages = array();
        $of_pages_obj = get_pages('sort_column=post_parent,menu_order');
        foreach ($of_pages_obj as $of_page) {
            $of_pages[$of_page->ID] = $of_page->post_name;
        }
        $of_pages_tmp = array_unshift($of_pages, "Sayfa seçiniz:");

// Image Alignment radio box
        $options_thumb_align = array("alignleft" => "Left", "alignright" => "Right", "aligncenter" => "Center");

// Image Links to Options
        $options_image_link_to = array("image" => "The Image", "post" => "The Post");

// Number of featured posts to display
        $featured_options_select = array("2", "4", "6", "8", "10", "12");

//Stylesheets Reader
        $alt_stylesheet_path = OF_FILEPATH . '/styles/';
        $alt_stylesheets = array();

        if (is_dir($alt_stylesheet_path)) {
            if ($alt_stylesheet_dir = opendir($alt_stylesheet_path)) {
                while (($alt_stylesheet_file = readdir($alt_stylesheet_dir)) !== false) {
                    if (stristr($alt_stylesheet_file, ".css") !== false) {
                        $alt_stylesheets[] = $alt_stylesheet_file;
                    }
                }
            }
        }

//More Options
        $uploads_arr = wp_upload_dir();
        $all_uploads_path = $uploads_arr['path'];
        $all_uploads = get_option('of_uploads');
        $thumbsekil = array("Solda", "Şerit");
// Set the Options Array
        $options = array();

        //General Settings

        $options[] = array("name" => "Genel Ayarlar",
            "type" => "heading");


        $options[] = array("name" => "Meta Keywords",
            "desc" => "Site etiketlerini girin.SEO için önemlidir.(En fazla 6 tane olmalıdır. Araya virgül koyarak girin.)",
            "id" => $shortname . "_keyword",
            "std" => "",
            "type" => "text");

        $options[] = array("name" => "Favicon",
            "desc" => "Url giriniz",
            "id" => $shortname . "_favicon",
            "std" => "",
            "type" => "text");


        //Header

        $options[] = array("name" => "Header",
            "type" => "heading");

        $options[] = array("name" => "Logo",
            "desc" => "logo (url) girin",
            "id" => $shortname . "_logo",
            "std" => "",
            "type" => "text");

        $options[] = array("name" => "Free Appointment",
        "desc" => "Url giriniz",
        "id" => $shortname . "_free_app_url",
        "std" => "",
        "type" => "text");

        $options[] = array("name" => "Phone",
            "desc" => "Telefon numarası giriniz",
            "id" => $shortname . "_phone",
            "std" => "",
            "type" => "text");

        $options[] = array("name" => "Icon",
            "desc" => "Örn: fab fa-whatsapp - iconlar için https://fontawesome.com",
            "id" => $shortname . "_phone_icon",
            "std" => "",
            "type" => "text");

        //Slider

        $options[] = array("name" => "Slider",
            "type" => "heading");

        $options[] = array("name" => "Üst Button",
            "desc" => "Button Adı",
            "id" => $shortname . "_home_slider_top_button",
            "std" => "",
            "type" => "text");

        $options[] = array("name" => "Button URL",
            "desc" => "Button URL",
            "id" => $shortname . "_home_slider_top_button_url",
            "std" => "",
            "type" => "text");

        $options[] = array("name" => "Home Slider Slogan",
            "desc" => "Slogan girin",
            "id" => $shortname . "_home_slider_slogan",
            "std" => "",
            "type" => "text");

        $options[] = array("name" => "Alt Button",
            "desc" => "Button Adı",
            "id" => $shortname . "_home_slider_bottom_button",
            "std" => "",
            "type" => "text");

        $options[] = array("name" => "Button URL",
            "desc" => "Button URL",
            "id" => $shortname . "_home_slider_bottom_button_url",
            "std" => "",
            "type" => "text");

        //Sosyal Medya

        $options[] = array("name" => "Social Media",
            "type" => "heading");


        $options[] = array("name" => "Facebook",
            "desc" => "Örn: estetikinternationaleng",
            "id" => $shortname . "_facebook",
            "std" => "",
            "type" => "text");

        $options[] = array("name" => "Twitter",
            "desc" => "Örn: estetikinternationaleng",
            "id" => $shortname . "_twitter",
            "std" => "",
            "type" => "text");

        $options[] = array("name" => "Instagram",
            "desc" => "Örn: estetikinternationaleng",
            "id" => $shortname . "_instagram",
            "std" => "",
            "type" => "text");

            
            
        // //Pages Background
        // $options[] = array("name" => "Pages",
        // "type" => "heading");

        // $options[] = array("name" => "Real Stores",
        //             "desc" => "Background link",
        //             "id" => $shortname . "_real_stores_bg",
        //             "std" => "",
        //             "type" => "text");
        

         //Stats

         $options[] = array("name" => "Stats",
         "type" => "heading");
         
         $options[] = array("name" => "Total Operation",
            "desc" => "Değer giriniz",
            "id" => $shortname . "_total",
            "std" => "",
            "type" => "text");

            $options[] = array("name" => "Our Patient Count",
            "desc" => "Değer giriniz",
            "id" => $shortname . "_our_count",
            "std" => "",
            "type" => "text");

            $options[] = array("name" => "Male Patient Count",
            "desc" => "Değer giriniz",
            "id" => $shortname . "_male_count",
            "std" => "",
            "type" => "text");

            $options[] = array("name" => "Patient Count",
            "desc" => "Değer giriniz",
            "id" => $shortname . "_patient_count",
            "std" => "",
            "type" => "text");

            $options[] = array("name" => "Surgical Operation",
            "desc" => "Değer giriniz",
            "id" => $shortname . "_surgical",
            "std" => "",
            "type" => "text");

            $options[] = array("name" => "Hair Planting number",
            "desc" => "Değer giriniz",
            "id" => $shortname . "_hair",
            "std" => "",
            "type" => "text");



//   Footer

        $options[] = array("name" => "Footer",
            "type" => "heading");

        $options[] = array("name" => "İletişim",
            "desc" => "Adres giriniz",
            "id" => $shortname . "_home_contact",
            "std" => "",
            "type" => "textarea");

        $options[] = array("name" => "Icon 1",
            "desc" => "Url giriniz",
            "id" => $shortname . "_home_contact_icon1",
            "std" => "",
            "type" => "text");

        $options[] = array("name" => "Icon 2",
            "desc" => "Url giriniz",
            "id" => $shortname . "_home_contact_icon2",
            "std" => "",
            "type" => "text");


        $options[] = array("name" => "Tüm Haklar (Copyright)",
            "desc" => "Adres giriniz",
            "id" => $shortname . "_copyright",
            "std" => "",
            "type" => "text");


           






        update_option('of_template', $options);
        update_option('of_themename', $themename);
        update_option('of_shortname', $shortname);

    }
}
?>