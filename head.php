<!DOCTYPE html>
<html lang="<?php bloginfo("language"); ?>">
<head>
    <title><?php bloginfo('name'); ?> <?php wp_title('-'); ?> | <?php bloginfo( 'description' ); ?></title>
    <meta charset="<?php bloginfo( 'charset' ); ?>" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="<?php bloginfo( 'description' ); ?>" />
    <meta name="keywords" content="<?php echo get_option('est_keyword'); ?>" />

    <!-- Favicon -->
    <link rel="shortcut icon" href="<?php echo get_option('est_favicon'); ?>">

    <!-- Bootstrap -->
    <link href="<?php bloginfo('template_url'); ?>/dist/css/custom.css" rel="stylesheet">
    <link href="<?php bloginfo('template_url'); ?>/dist/css/bootstrap.css" rel="stylesheet">
    <link href="<?php bloginfo('template_url'); ?>/dist/css/owl.carousel.min.css" rel="stylesheet">
    <link href="<?php bloginfo('template_url'); ?>/dist/css/fancybox.min.css" rel="stylesheet">


    <!-- Font-->
    <script src="https://unpkg.com/feather-icons/dist/feather.min.js"></script>
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php wp_head(); ?>
</head>
<body>







