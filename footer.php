<footer id="sub_footer">
      <div class="container-fluid full-width">
        <div class="row">
          <div class="col-lg-4 col-md-6 col-xs-12 social-box-item"><span class="flex"><i data-feather="twitter"></i><span><img src="<?php bloginfo('template_url'); ?>/dist/img/footer/social/twitter.png" alt=""><small>twitter/<b>EstetikArabic</b></small></span></span><a href="<?php echo get_option('est_twitter') ?>">Follow</a></div>
          <div class="col-lg-4 col-md-6 col-xs-12 social-box-item"><span class="flex"><i data-feather="instagram"></i><span><img src="<?php bloginfo('template_url'); ?>/dist/img/footer/social/instagram.png" alt=""><small>instagram/<b>EstetikArabic</b></small></span></span><a href="<?php echo get_option('est_instagram') ?>">Follow</a></div>
          <div class="col-lg-4 col-md-6 col-xs-12 social-box-item"><span class="flex"><i data-feather="facebook"></i><span><img src="<?php bloginfo('template_url'); ?>/dist/img/footer/social/facebook.png" alt=""><small>facebook/<b>EstetikArabic</b></small></span></span><a href="<?php echo get_option('est_facebook') ?>">Follow</a></div>
        </div>
      </div>
      <div class="space_35 hidden-md hidden-xs visable-lg"></div>
      <div>
        <div class="main">
          <div class="top-group container-fluid">
            <div class="col-xs-12 col-sm-3 col-md-3 top_one">
              <h3>OUR DOCTORS</h3>
              <ul>
                <li><a href="#">BÜLENT CİHANTİMUR MD</a></li>
                <li><a href="#">YÜCEL SARIALTIN MD</a></li>
                <li><a href="#">RIZA KANTÜRK MD</a></li>
                <li><a href="#">ALİ EKBER YÜREKLİ MD</a></li>
                <li><a href="#">GÜLDEN GÜREL MD</a></li>
                <li><a href="#">FEYMAN DUYGU OKTAR MD.</a></li>
                <li><a href="#">DR. CEMAL BEKTAŞ</a></li>
                <li><a href="#">OP.DR. MEDİNE R. KANTÜRK</a></li>
                <li><a href="#">DR. ÇAĞATAY GÜNGÖRSÜN</a></li>
                <li><a href="#">OP.DR.MURAT MELİH CAN </a></li>
                <li><a href="#">OP.DR.MEHMET CAN</a></li>
                <li><a href="#">SAKİ AZİZ AKSÖZ</a></li>
              </ul>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 top_two hidden-xs">
              <h3>OUR TECHNIQUES</h3>
              <ul>
                <li><a href="#">ORGANIC HAIR TRANSPLANTATION</a></li>
                <li><a href="#">SIMPLE RHINOPLASTY MD</a></li>
                <li><a href="#">INCISIONLESS OTOPLASTY</a></li>
                <li><a href="#"> CIHANTIMUR FAT TRANSFER</a></li>
              </ul>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 top_three hidden-xs">
              <h3>OUR CLINICS</h3>
              <ul>
                <li><a href="#">İSTANBUL BYOMED</a></li>
                <li><a href="#">ANKARA MD</a></li>
                <li><a href="#">İZMİR</a></li>
                <li><a href="#">BURSA KORUPARK</a></li>
                <li><a href="#">BURSA ZAFER PLAZA</a></li>
                <li><a href="#">PLAZA BURSA ÇEKİRGE</a></li>
                <li><a href="#">OUR PARTNERS</a></li>
              </ul>
            </div>
            <div class="col-xs-12 col-sm-3 col-md-3 top_four hidden-xs">
              <h3>CONTACT US</h3>
              <p>Estetik International Byomed Saniye Ermutlu Sok. Kozyatağı İstanbul / Turkey <br><br>                        info@estetikinternational.com <br><br>                        +90 444 77 07 | +90 543 968 99 88</p><a class="hidden-xs" href="#"><img class="img-responsive col-md-3" src="<?php bloginfo('template_url'); ?>/dist/img/footer/joint.png" alt="joint"></a><a class="hidden-xs" href="#"><img class="img-responsive col-md-3" style="min-width: 30%;" src="<?php bloginfo('template_url'); ?>/dist/img/footer/realself.png" alt="realself"></a>
            </div>
            <div class="space_80"></div>
          </div>
        </div>
        <!-- Mobil Panel-->
        <div class="panel-group visible-xs" id="accordion" role="tablist" aria-multiselectable="true">
          <div class="panel panel-default">
            <div class="panel-heading" id="headingOne" role="tab">
              <h4 class="panel-title"><a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">OUR TECHNIQUES</a><a class="plus pull-right" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">+</a></h4>
            </div>
            <div class="panel-collapse collapse" id="collapseOne" role="tabpanel" aria-labelledby="headingOne">
              <div class="panel-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon
                officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan
                excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                you probably haven't heard of them accusamus labore sustainable VHS.
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" id="headingTwo" role="tab">
              <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">OUR CLINICS</a><a class="plus pull-right" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">+</a></h4>
            </div>
            <div class="panel-collapse collapse" id="collapseTwo" role="tabpanel" aria-labelledby="headingTwo">
              <div class="panel-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon
                officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan
                excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                you probably haven't heard of them accusamus labore sustainable VHS.
              </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading" id="headingThree" role="tab">
              <h4 class="panel-title"><a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">CONTACT US</a><a class="plus pull-right" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">+</a></h4>
            </div>
            <div class="panel-collapse collapse" id="collapseThree" role="tabpanel" aria-labelledby="headingThree">
              <div class="panel-body">
                Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon
                officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3
                wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et.
                Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan
                excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt
                you probably haven't heard of them accusamus labore sustainable VHS.
              </div>
            </div>
          </div>
        </div>
        <!-- #block
        .container-fluid.email-area
          .row
            .col-xs-12.col-md-6
              .col-xs-12.col-md-12
                h5
                  span.blok_text E-Mail
                  |  Subscription
              .col-xs-12.col-md-6
                h4.bloktwo_text Sign up for our plastic surgery newsletter
            .col-md-6
              form
                input.col-xs-8.col-sm-10.col-md-6.email(type='email', value='Enter your e-mail address')
                input#submit.col-xs-2(type='submit', value='OK')
        .clearfix
        -->
      </div>
      <!-- Mobil Panel-->
      <div id="footer_bottom">
        <div class="f_logo pull-right">
          <div class="space_50"></div><img src="<?php bloginfo('template_url'); ?>/dist/img/footer/estetik_logo_footer.png" width="54" alt="">
        </div>
        <div class="space_50 hidden-xs"></div>
        <div class="col-lg-12 col-xs-12 text-center f_menu hidden-xs">
        <?php
           wp_nav_menu( array(
            'theme_location'	=> 'footer-menu',
            'depth'				=> 1, // 1 = with dropdowns, 0 = no dropdowns.
            'container_id'		=> 'bs-example-navbar-collapse-1',
            'menu_class'		=> 'list-inline',
            'fallback_cb'		=> 'WP_Bootstrap_Navwalker::fallback',
            'walker'			=> new WP_Bootstrap_Navwalker()
        ) );
        ?>
         </div>
        <div class="space_25"></div>
        <div class="divider"></div>
        <div class="space_40"></div>
        <h4 class="text-center"><?php echo get_option('est_copyright') ?></h4>
        <div class="space_40"></div>
        <div class="col-lg-12 col-xs-12 text-center">
          <ul class="list-inline mini_menu">
            <li class="list-inline-item"><a href="/privacy_policy">Privacy Policy</a></li>
            <li class="list-inline-item"><a href="/campany">Company</a></li>
            <li class="list-inline-item"><a href="/contact">Contact</a></li>
          </ul>
        </div>
        <div class="space_50"></div>
      </div>
    </footer>



<?php wp_footer(); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed-->
<script src="<?php bloginfo('template_url'); ?>/dist/js/bootstrap.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/dist/js/owl.carousel.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/dist/js/fancybox.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/dist/js/main.js"></script>
</body>
</html>

